﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DbProviderHelper
{
    public class DataFactory
    {
        static DbHelper db;
        static DataFactory()
        {
            db = DbHelper.getDbFactory(DbProviderType.OleDb, System.Configuration.ConfigurationManager.AppSettings["connString"].ToString());
        }

        #region 执行sql语句

        /// <summary>
        /// 根据传入的CommandType来确定执行sql还是存储过程
        /// </summary>
        /// <param name="commandType"></param>
        /// <param name="sql"></param>
        /// <param name="para"></param>
        /// <returns></returns>
        public static int ExecuteNonQuery(CommandType commandType, string sql, params DbParameter[] para)
        {
            return db.ExecuteNonQuery(commandType, sql, para);
        }
        /// <summary>
        /// 执行sql语句
        /// </summary>
        /// <param name="sql">查询sql</param>
        /// <param name="para">查询参数</param>
        /// <returns></returns>
        public static int ExecuteNonQuery(string sql, params DbParameter[] para)
        {
            return db.ExecuteNonQuery(sql, para);
        }

        #endregion

        #region 执行sql查询，返回首行首列
        /// <summary>
        /// 执行sql或存储过程返回首行首列
        /// </summary>
        /// <param name="commandType"></param>
        /// <param name="sql"></param>
        /// <param name="para"></param>
        /// <returns></returns>
        public static object ExecuteScaler(CommandType commandType, string sql, params DbParameter[] para)
        {
            return db.ExecuteScaler(commandType, sql, para);
        }

        /// <summary>
        /// 执行sql语句查询返回首行首列的值
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="para"></param>
        /// <returns></returns>
        public static object ExecuteScaler(string sql, params DbParameter[] para)
        {
            return db.ExecuteScaler(CommandType.Text, sql, para);
        }

        #endregion

        #region 执行sql查询，返回DataTable
        /// <summary>
        /// 执行sql语句或存储过程查询返回结果集的DataTable
        /// </summary>
        /// <param name="commandType"></param>
        /// <param name="sql"></param>
        /// <param name="para"></param>
        /// <returns></returns>
        public static DataTable getDataTable(CommandType commandType, string sql, params DbParameter[] para)
        {
            return db.getDataTable(commandType, sql, para);
        }
        /// <summary>
        /// 执行sql语句查询返回结果集的DataTable
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="para"></param>
        /// <returns></returns>
        public static DataTable getDataTable(string sql, params DbParameter[] para)
        {
            return db.getDataTable(sql, para);
        }

        #endregion

        #region getDataSet

        /// <summary>
        /// 执行sql语句查询返回结果集的DataSet
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="para"></param>
        /// <returns></returns>
        public static DataSet getDataSet(string sql, params DbParameter[] para)
        {
            return db.getDataSet(sql, para);
        }

        /// <summary>
        /// 执行sql语句或存储过程查询返回结果集的DataSet
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="para"></param>
        /// <returns></returns>
        public static DataSet getDataSet(CommandType commandType, string sql, params DbParameter[] para)
        {
            return db.getDataSet(commandType, sql, para);
        }

        #endregion

        /// <summary>
        /// 将一个对象中的数据保存在数据库中
        /// </summary>
        /// <param name="obj">需要保存的对象</param>
        /// <param name="autoGrowKeyName">自动增加的字段名</param>
        public static void SaveObject(object obj, string autoGrowKeyName)
        {
            Type t = obj.GetType();
            string filed = String.Empty;
            string values = String.Empty;
            string sqlStr = "insert into " + t.Name + " (";
            int pLength = 0;
            if (String.IsNullOrEmpty(autoGrowKeyName))
                pLength = t.GetProperties().Length;
            else
                pLength = t.GetProperties().Length - 1;
            int i = 0;
            foreach (System.Reflection.PropertyInfo pi in t.GetProperties())
            {
                string AttName = pi.Name;
                if (AttName == autoGrowKeyName)
                    continue;
                filed += AttName + ",";
                values += "@" + AttName + ",";

            }
            sqlStr += filed.Substring(0, filed.Length - 1) + ") values(" + values.Substring(0, values.Length - 1) + ")";
            SqlParameter[] parameters = new SqlParameter[pLength];
            foreach (System.Reflection.PropertyInfo pi in t.GetProperties())
            {
                string AttName = pi.Name;
                if (AttName == autoGrowKeyName)
                {
                    continue;
                }
                object o = pi.GetValue(obj, null);
                parameters[i] = new SqlParameter("@" + AttName, o);
                i++;
            }
            DataFactory.ExecuteScaler(CommandType.Text, sqlStr, parameters);

        }
    }
}
