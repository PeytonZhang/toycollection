﻿using AzureEncryptionExtensions;
using AzureEncryptionExtensions.Providers;
using DbProviderHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncryptFileLibrary
{
    class Program
    {
        static void Main(string[] args)
        {
            //string keyFolder = AppDomain.CurrentDomain.BaseDirectory + @"\\KeyStorage";
            //if (!System.IO.Directory.Exists(keyFolder))
            //    System.IO.Directory.CreateDirectory(keyFolder);

            //SymmetricBlobCryptoProvider provider = new SymmetricBlobCryptoProvider();
            //provider.WriteKeyFile(AppDomain.CurrentDomain.BaseDirectory + "\\KeyStorage\\symmetrickey.pfx");

            //AsymmetricBlobCryptoProvider provider1 = new AsymmetricBlobCryptoProvider();
            //provider1.WriteKeyFile(AppDomain.CurrentDomain.BaseDirectory + "\\KeyStorage\\asymmectric_public.cer", true);
            //provider1.WriteKeyFile(AppDomain.CurrentDomain.BaseDirectory + "\\KeyStorage\\asymmectrickey.pfx");

            IBlobCryptoProvider cryptoProvider = AzureEncryptionExtensions.ProviderFactory.CreateProviderFromKeyFile("D:\\symmetrickey.pfx");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Stream encryptedStream = cryptoProvider.EncryptedStream(ReadSingleFile(@"D:\Codes\respoestories\ToyForBit\toycollection\EncryptFileLibrary\EncryptTool\Encryption.mdb"));
            WriteSignleFile(@"D:\Codes\respoestories\ToyForBit\toycollection\EncryptFileLibrary\EncryptTool\bin\Debug\Encryption.mdb", encryptedStream);
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds.ToString());

            //sw.Restart();
            //Stream decryptedStream = cryptoProvider.DecryptedStream(ReadSingleFile(@"D:\fripSide.mkv"));
            //WriteSignleFile(@"D:\decryptedfripSide.mkv", decryptedStream);
            //sw.Stop();
            //Console.WriteLine(sw.ElapsedMilliseconds.ToString());

            //sw.Start();
            //File.Copy(@"D:\Downloads\fripSide专场LIVE\fripside 2012 decade tokyo LIVE\[Live] fripSide 10th Anniversary Live 2012 ~Decade Tokyo~ (BD_1080p_Hi10p_FLAC_chapter).mkv", @"D:\abc.mkv");
            //sw.Stop();
            //Console.WriteLine(sw.ElapsedMilliseconds.ToString());


            Console.ReadKey();
        }

        private static Stream ReadSingleFile(string filePath)
        {
            FileStream sourceStream = null;
            try {
                sourceStream = new FileStream(filePath, FileMode.Open);
            } catch (IOException)
            {
                throw;
            }

            return sourceStream;
        }

        private static void WriteSignleFile(string filePath, Stream sourceStream)
        {
            //写入本地文件(开始)
            int fileSize = 0;
            FileStream targetStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None);

            //定义文件缓冲区
            const int bufferLen = 10240;
            byte[] buffer = new byte[bufferLen];
            int count = 0;

            while ((count = sourceStream.Read(buffer, 0, bufferLen)) > 0)
            {
                targetStream.Write(buffer, 0, count);
                fileSize += count;
            }
            targetStream.Flush();
            targetStream.Close();
            //写入本地文件(结束)

            //关闭加密后的文件流
            sourceStream.Close();
        }

        static void Main()
        {
            using (var aes = System.Security.Cryptography.Aes.Create())
            {
                var data = Encoding.Unicode.GetBytes("Mgen!");
                Console.WriteLine(BitConverter.ToString(Encrypt_Write(aes.CreateEncryptor(), data).ToArray()));
            }
        }

        static MemoryStream Encrypt_Write(System.Security.Cryptography.ICryptoTransform ict, byte[] data)
        {
            using (var ms = new MemoryStream())
            using (var cstream = new System.Security.Cryptography.CryptoStream(ms, ict, System.Security.Cryptography.CryptoStreamMode.Write))
            {
                cstream.Write(data, 0, data.Length);
                return ms;
            }
        }
    }
}
