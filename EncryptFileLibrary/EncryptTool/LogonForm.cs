﻿using AzureEncryptionExtensions.Providers;
using DbProviderHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EncryptTool
{
    public partial class LogonForm : Form
    {
        public LogonForm()
        {
            InitializeComponent();
        }

        public const string BLOBPATH = "BlobData";

        private void btnSelect_Click(object sender, EventArgs e)
        {
            OpenFileDialog oFD = new OpenFileDialog();
            oFD.Title = "打开文件";
            oFD.ShowHelp = true;
            oFD.Filter = "证书文件|*.pfx";//过滤格式
            oFD.FilterIndex = 1;                                    //格式索引
            oFD.RestoreDirectory = false;
            oFD.InitialDirectory = "c:\\";                          //默认路径
            oFD.Multiselect = false;                                 //是否多选
            
            if (oFD.ShowDialog() == DialogResult.OK)
            {
                string fileName = oFD.FileName;

                this.txtCridention.Text = fileName;
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string fileName = this.txtCridention.Text;

            if (!Directory.Exists(BLOBPATH))
                Directory.CreateDirectory(BLOBPATH);
            string blobFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, BLOBPATH);
            string mdbPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Encryption.mdb");
            if (!File.Exists(mdbPath))
            {
                FileInfo fi = new FileInfo(blobFolder + @"\Encryption.mdb");
                if (fi != null && fi.Exists)
                {
                    IBlobCryptoProvider cryptoProvider = AzureEncryptionExtensions.ProviderFactory.CreateProviderFromKeyFile(fileName);
                    Stream decryptedStream = cryptoProvider.DecryptedStream(ReadSingleFile(fi.FullName));
                    WriteSignleFile(mdbPath, decryptedStream);
                }
                
            }

            string downloadPath = String.Empty;
            if (canAccess(ref downloadPath))
            {
                EncrytFileForm fileForm = new EncrytFileForm();
                fileForm.KeyPath = fileName;
                fileForm.DownloadPath = downloadPath;
                fileForm.Show();
                this.Visible = false;
            }
            else
            {
                waring.Visible = true;
            }

        }

        private bool canAccess(ref string downloadPath)
        {
            try
            {
                DataTable dt = DataFactory.getDataTable("select * from Logon");
                if (dt != null && dt.Rows.Count > 0)
                {
                    downloadPath = dt.Rows[0]["DownloadPath"].ToString();

                    return true;

                }
                else
                    return false;

            }
            catch(Exception ex)
            {
                return false;
            }
            
        }

        private static Stream ReadSingleFile(string filePath)
        {
            FileStream sourceStream = null;
            try
            {
                sourceStream = new FileStream(filePath, FileMode.Open);
            }
            catch (IOException)
            {
                throw;
            }

            return sourceStream;
        }

        private static void WriteSignleFile(string filePath, Stream sourceStream)
        {
            //写入本地文件(开始)
            int fileSize = 0;
            FileStream targetStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None);

            //定义文件缓冲区
            const int bufferLen = 10240;
            byte[] buffer = new byte[bufferLen];
            int count = 0;

            while ((count = sourceStream.Read(buffer, 0, bufferLen)) > 0)
            {
                targetStream.Write(buffer, 0, count);
                fileSize += count;
            }
            targetStream.Flush();
            targetStream.Close();
            //写入本地文件(结束)

            //关闭加密后的文件流
            sourceStream.Close();
        }


    }
}
