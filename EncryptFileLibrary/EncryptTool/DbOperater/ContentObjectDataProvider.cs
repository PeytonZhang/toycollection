﻿using DbProviderHelper;
using EncryptTool.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncryptTool.DbOperater
{
    public class ContentObjectDataProvider
    {
        public static List<ContentObject> GetContentObjectByParentID(int parentId)
        {
            try
            {
                DataTable contentDt = DataFactory.getDataTable("select ID,ParentID,ObjectName,ObjectType,ObjectExtension,ObjectSize,ObjectLastModify,ObjectFullPath,ObjectEncryptedName from ContentObject where ParentID = "+ parentId);
                List<ContentObject> contentObjects = ConvertContentObjectFromDataTable(contentDt);
                return contentObjects;
            }catch(Exception ex)
            {
                throw;
            }
            
        }

        public static ContentObject GetContentObjectByFullPath(string fullPath)
        {
            try
            {
                DataTable contentDt = DataFactory.getDataTable("select ID,ParentID,ObjectName,ObjectType,ObjectExtension,iif(ObjectSize=0,' ',ObjectSize) as ObjectSize,ObjectLastModify,ObjectFullPath,ObjectEncryptedName from ContentObject where ObjectFullPath = '" + fullPath + "'");
                List<ContentObject> contentObjects = ConvertContentObjectFromDataTable(contentDt);
                return contentObjects.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static ContentObject GetContentObjectByID(int Id)
        {
            try
            {
                DataTable contentDt = DataFactory.getDataTable("select ID,ParentID,ObjectName,ObjectType,ObjectExtension,iif(ObjectSize=0,' ',ObjectSize) as ObjectSize,ObjectLastModify,ObjectFullPath,ObjectEncryptedName from ContentObject where ID = " + Id);
                List<ContentObject> contentObjects = ConvertContentObjectFromDataTable(contentDt);
                return contentObjects.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        private static List<ContentObject> ConvertContentObjectFromDataTable(DataTable contentDt)
        {
            List<ContentObject> contentObjects = new List<ContentObject>();
            foreach (DataRow dr in contentDt.Rows)
            {
                ContentObject contentObject = new ContentObject();

                contentObject.ID = int.Parse(isNotNull(dr["ID"], 0).ToString());
                contentObject.ParentID = int.Parse(isNotNull(dr["ParentID"], 0).ToString());
                contentObject.ObjectType = isNotNull(dr["ObjectType"], "").ToString();
                contentObject.ObjectSize = long.Parse(isNotNull(dr["ObjectSize"], 0).ToString());
                contentObject.ObjectName = isNotNull(dr["ObjectName"], "").ToString();
                contentObject.ObjectLastModify = DateTime.Parse(isNotNull(dr["ObjectLastModify"], DateTime.Now).ToString());
                contentObject.ObjectFullPath = isNotNull(dr["ObjectFullPath"], "").ToString();
                contentObject.ObjectExtension = isNotNull(dr["ObjectExtension"], 0).ToString();
                contentObject.ObjectEncryptedName = isNotNull(dr["ObjectEncryptedName"], "").ToString();
                contentObjects.Add(contentObject);
            }
            return contentObjects;
        }

        private static object isNotNull(object obj,object defaltValue)
        {
            if(obj == null || obj == DBNull.Value || String.IsNullOrWhiteSpace(obj.ToString()))
            {
                return defaltValue;
            }
            else
            {
                return obj;
            }
        }

        public static void InsertContentObject(ContentObject contentObject)
        {
            try
            {
                DataFactory.ExecuteNonQuery("insert into ContentObject(ObjectName,ObjectType,ObjectExtension,ObjectSize,ObjectLastModify,ParentID,ObjectFullPath,ObjectEncryptedName) value('" + contentObject.ObjectName + "','" + contentObject.ObjectType + "','" + contentObject.ObjectExtension + "'," + contentObject.ObjectSize + "," + contentObject.ObjectLastModify + "," + contentObject.ParentID + ",'" + contentObject.ObjectFullPath + "')");

            }catch(Exception ex)
            {
                throw;
            }

        }
    }
}
