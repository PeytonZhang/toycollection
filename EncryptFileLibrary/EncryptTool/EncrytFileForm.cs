﻿using AzureEncryptionExtensions.Providers;
using DbProviderHelper;
using EncryptTool.DbOperater;
using EncryptTool.Model;
using EncryptTool.Properties;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace EncryptTool
{
    public partial class EncrytFileForm : Form
    {
        public EncrytFileForm()
        {
            InitializeComponent();
        }

        public string KeyPath = "";
        public const string BLOBPATH = "BlobData";
        public string DownloadPath = "";

        private void EncrytFileForm_Load(object sender, EventArgs e)
        {
            List<ContentObject> contentObjects = ContentObjectDataProvider.GetContentObjectByParentID(1);
            this.dataGridView1.DataSource = contentObjects;
            this.txtFullPath.Text = "/";
        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void tsmi_Upload_Click(object sender, EventArgs e)
        {
            ContentObject contentObject = contentObject = ContentObjectDataProvider.GetContentObjectByFullPath(this.txtFullPath.Text);

            OpenFileDialog oFD = new OpenFileDialog();
            oFD.Title = "打开文件";
            oFD.ShowHelp = true;
            oFD.Filter = "所有文件|*.*";//过滤格式
            oFD.FilterIndex = 1;                                    //格式索引
            oFD.RestoreDirectory = false;
            oFD.InitialDirectory = "c:\\";                          //默认路径
            oFD.Multiselect = true;                                 //是否多选
            string[] fileNames = null;
            if (oFD.ShowDialog() == DialogResult.OK)
                fileNames = oFD.FileNames;

            uploadFile(fileNames, contentObject.ID);

            List<ContentObject> contentObjects = ContentObjectDataProvider.GetContentObjectByParentID(contentObject.ID);

            this.dataGridView1.DataSource = contentObjects;

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dataGridView = (DataGridView)sender;
            if (e.RowIndex < 0 || e.ColumnIndex != 3)
                return;
            DataGridViewRow dataGridViewRow = dataGridView.Rows[e.RowIndex];
            if (dataGridViewRow.Cells["ObjectType"].Value.ToString() == "folder")
            {
                string objectName = dataGridViewRow.Cells["ObjectName"].Value.ToString();
                int Id = int.Parse(dataGridViewRow.Cells["ID"].Value.ToString());
                List<ContentObject> contentDt = ContentObjectDataProvider.GetContentObjectByParentID(Id);;
                this.dataGridView1.DataSource = contentDt;

                this.txtFullPath.Text = this.txtFullPath.Text + objectName + "/";
            }
            
        }

        private void uploadFile(string[] filePathCollection, int parentId = 1)
        {
            if (filePathCollection == null) return;

            foreach(string filePath in filePathCollection)
            {
                FileInfo file = new FileInfo(filePath);
                string fullPath = this.txtFullPath.Text + file.Name;
                string encryptedName = encryptFile(file, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, BLOBPATH));
                DataFactory.ExecuteNonQuery("insert into ContentObject(ObjectName,ObjectType,ObjectExtension,ObjectSize,ParentID,ObjectFullPath,ObjectEncryptedName) values('"+ file.Name+ "','file','"+ file.Extension + "','"+file.Length+"',"+parentId+",'"+ fullPath + "','"+ encryptedName + "')");
            }

        }

        private void downloadFile(string filename,string fileEncryptedName,string targetFolder)
        {
            try
            {
                string blobEncryptedName = Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, BLOBPATH), fileEncryptedName);
                string targetFilePath = Path.Combine(targetFolder, filename);
                decryptFile(blobEncryptedName, targetFilePath);
            }catch(Exception ex)
            {
                throw;
            }
            
        }

        private string encryptFile(FileInfo file,string blobDataPath)
        {
            string encryptedName = Guid.NewGuid().ToString();
            IBlobCryptoProvider cryptoProvider = AzureEncryptionExtensions.ProviderFactory.CreateProviderFromKeyFile(KeyPath);
            
            Stream encryptedStream = cryptoProvider.EncryptedStream(ReadSingleFile(file.FullName));
            WriteSignleFile(Path.Combine(blobDataPath,encryptedName), encryptedStream);
            return encryptedName;
        }

        private void decryptFile(string blobEncryptedName, string targetFilePath)
        {

            IBlobCryptoProvider cryptoProvider = AzureEncryptionExtensions.ProviderFactory.CreateProviderFromKeyFile(KeyPath);
            Stream decryptedStream = cryptoProvider.DecryptedStream(ReadSingleFile(blobEncryptedName));
            WriteSignleFile(targetFilePath, decryptedStream);
        }

        private static Stream ReadSingleFile(string filePath)
        {
            FileStream sourceStream = null;
            try
            {
                sourceStream = new FileStream(filePath, FileMode.Open);
            }
            catch (IOException)
            {
                throw;
            }

            return sourceStream;
        }

        private static void WriteSignleFile(string filePath, Stream sourceStream)
        {
            //写入本地文件(开始)
            int fileSize = 0;
            FileStream targetStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None);

            //定义文件缓冲区
            const int bufferLen = 10240;
            byte[] buffer = new byte[bufferLen];
            int count = 0;
            
            while ((count = sourceStream.Read(buffer, 0, bufferLen)) > 0)
            {
                targetStream.Write(buffer, 0, count);
                fileSize += count;
            }
            targetStream.Flush();
            targetStream.Close();
            //写入本地文件(结束)

            //关闭加密后的文件流
            sourceStream.Close();
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name.Equals("ObjectIcon"))
            {
               
                string objectType = dataGridView1.Rows[e.RowIndex].Cells["ObjectType"].Value.ToString().ToLowerInvariant();
                switch (objectType)
                {
                    case "folder":
                        e.Value = Resources.folder;
                        break;
                    default:
                        e.Value = Resources.file;
                        break;
                }
            }

        }

        private void tsmi_NewFolder_Click(object sender, EventArgs e)
        {
            RemaneForm rename = new RemaneForm();
            DialogResult result = rename.ShowDialog();
            
            if(result == DialogResult.OK)
            {
                ContentObject contentObject = contentObject = ContentObjectDataProvider.GetContentObjectByFullPath(this.txtFullPath.Text);
                string fullPath = this.txtFullPath.Text + rename.folderName + "/";
                DataFactory.ExecuteNonQuery("insert into ContentObject(ObjectName,ObjectType,ObjectExtension,ObjectSize,ParentID,ObjectFullPath,ObjectEncryptedName) values('" + rename.folderName + "','folder',' ',0," + contentObject.ID + ",'" + fullPath + "',' ')");

                List<ContentObject> contentObjects = ContentObjectDataProvider.GetContentObjectByParentID(contentObject.ID);

                this.dataGridView1.DataSource = contentObjects;
            }

        }

        private void tsmi_Download_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(DownloadPath))
                {
                    foreach (DataGridViewRow dgvr in this.dataGridView1.Rows)
                    {
                        bool isSelected = (bool)dgvr.Cells[0].Selected;
                        if (isSelected)
                        {
                            if (dgvr.Cells["ObjectType"].Value.ToString().ToLowerInvariant() == "folder")
                            {
                                string folderPath = Path.Combine(DownloadPath, dgvr.Cells["ObjectName"].Value.ToString());
                                if (!Directory.Exists(folderPath))
                                    Directory.CreateDirectory(folderPath);
                                List<ContentObject> contentObjects = ContentObjectDataProvider.GetContentObjectByParentID(int.Parse(dgvr.Cells["ID"].Value.ToString()));
                                foreach (ContentObject contentObject in contentObjects)
                                {
                                    downloadFile(contentObject.ObjectName, contentObject.ObjectEncryptedName, folderPath);
                                }

                            }
                            else
                            {
                                ContentObject contentObject = ContentObjectDataProvider.GetContentObjectByID(int.Parse(dgvr.Cells["ID"].Value.ToString()));

                                downloadFile(contentObject.ObjectName, contentObject.ObjectEncryptedName, DownloadPath);

                            }
                        }
                    }
                }
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            
        }

        private void tsmi_Close_Click(object sender, EventArgs e)
        {
            string blobEncryptedName = Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, BLOBPATH), "Encryption.mdb");
            decryptFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Encryption.mdb"), blobEncryptedName);
        }
    }
}
