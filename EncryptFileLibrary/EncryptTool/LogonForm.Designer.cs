﻿namespace EncryptTool
{
    partial class LogonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCridention = new System.Windows.Forms.TextBox();
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.waring = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "指定证书位置：";
            // 
            // txtCridention
            // 
            this.txtCridention.Location = new System.Drawing.Point(112, 55);
            this.txtCridention.Name = "txtCridention";
            this.txtCridention.Size = new System.Drawing.Size(356, 21);
            this.txtCridention.TabIndex = 1;
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(239, 101);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(75, 23);
            this.btnLogin.TabIndex = 2;
            this.btnLogin.Text = "登录";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(474, 53);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(75, 23);
            this.btnSelect.TabIndex = 2;
            this.btnSelect.Text = "选择";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // waring
            // 
            this.waring.AutoSize = true;
            this.waring.ForeColor = System.Drawing.Color.Red;
            this.waring.Location = new System.Drawing.Point(320, 106);
            this.waring.Name = "waring";
            this.waring.Size = new System.Drawing.Size(77, 12);
            this.waring.TabIndex = 0;
            this.waring.Text = "证书不正确。";
            this.waring.Visible = false;
            // 
            // LogonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 147);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.txtCridention);
            this.Controls.Add(this.waring);
            this.Controls.Add(this.label1);
            this.Name = "LogonForm";
            this.Text = "LogonForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCridention;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Label waring;
    }
}