﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EncryptTool
{
    public partial class RemaneForm : Form
    {
        public RemaneForm()
        {
            InitializeComponent();
        }

        public string folderName = "";

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            string folderName = this.textBox1.Text;
            this.folderName = folderName;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            this.Close();
        }
    }
}
