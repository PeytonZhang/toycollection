﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncryptTool.Model
{
    public class ContentObject
    {
        public int ID { get; set; }

        public string ObjectName { get; set; }

        public string ObjectType { get; set; }

        public string ObjectExtension { get; set; }

        public long ObjectSize { get; set; }

        public DateTime ObjectLastModify { get; set; }

        public int ParentID { get; set; }

        public string ObjectFullPath { get; set; }

        public string ObjectEncryptedName { get; set; }
    }
}
