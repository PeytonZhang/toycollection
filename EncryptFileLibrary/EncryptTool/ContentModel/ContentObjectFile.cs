﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncryptTool.ContentModel
{
    public class ContentObjectFile : ContentObjectBase
    {
        public string FileExtends { get; set; }

        public long FileSize { get; set; }


    }
}
