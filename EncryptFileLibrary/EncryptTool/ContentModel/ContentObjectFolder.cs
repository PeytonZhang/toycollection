﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncryptTool.ContentModel
{
    public class ContentObjectFolder : ContentObjectBase
    {
        public int SubFolderCount { get; set; }

        public int SubFileCount { get; set; }

    }
}
