﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncryptTool.ContentModel
{
    public class ContentObjectBase
    {
        public string Id { get; set; }

        public string ParentId { get; set; }

        public string ContentName { get; set; }

        public string ContentFullPath { get; set; }

        public int Level { get; set; }

        public DateTime LastModify { get; set; }
    }
}
