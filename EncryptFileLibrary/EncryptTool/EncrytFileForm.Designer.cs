﻿namespace EncryptTool
{
    partial class EncrytFileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsmi_Upload = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_Download = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmi_NewFolder = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.MultSelect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ObjectIcon = new System.Windows.Forms.DataGridViewImageColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ObjectName = new System.Windows.Forms.DataGridViewLinkColumn();
            this.ObjectSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ObjectLastModify = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ObjectType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ObjectExtension = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ObjectFullPath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ObjectEncryptedName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.下载ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.上传ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.删除ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.新建文件夹ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.复制ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.剪切ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.粘贴ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtFullPath = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.tsmi_Close = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmi_Upload,
            this.tsmi_Download,
            this.tsmi_Delete,
            this.tsmi_NewFolder,
            this.tsmi_Close});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(586, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tsmi_Upload
            // 
            this.tsmi_Upload.Name = "tsmi_Upload";
            this.tsmi_Upload.Size = new System.Drawing.Size(44, 21);
            this.tsmi_Upload.Text = "上传";
            this.tsmi_Upload.Click += new System.EventHandler(this.tsmi_Upload_Click);
            // 
            // tsmi_Download
            // 
            this.tsmi_Download.Name = "tsmi_Download";
            this.tsmi_Download.Size = new System.Drawing.Size(44, 21);
            this.tsmi_Download.Text = "下载";
            this.tsmi_Download.Click += new System.EventHandler(this.tsmi_Download_Click);
            // 
            // tsmi_Delete
            // 
            this.tsmi_Delete.Name = "tsmi_Delete";
            this.tsmi_Delete.Size = new System.Drawing.Size(44, 21);
            this.tsmi_Delete.Text = "删除";
            // 
            // tsmi_NewFolder
            // 
            this.tsmi_NewFolder.Name = "tsmi_NewFolder";
            this.tsmi_NewFolder.Size = new System.Drawing.Size(80, 21);
            this.tsmi_NewFolder.Text = "新建文件夹";
            this.tsmi_NewFolder.Click += new System.EventHandler(this.tsmi_NewFolder_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MultSelect,
            this.ObjectIcon,
            this.ID,
            this.ObjectName,
            this.ObjectSize,
            this.ObjectLastModify,
            this.ObjectType,
            this.ObjectExtension,
            this.ParentID,
            this.ObjectFullPath,
            this.ObjectEncryptedName});
            this.dataGridView1.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridView1.Location = new System.Drawing.Point(1, 51);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(582, 264);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            // 
            // MultSelect
            // 
            this.MultSelect.HeaderText = "";
            this.MultSelect.Name = "MultSelect";
            this.MultSelect.Width = 30;
            // 
            // ObjectIcon
            // 
            this.ObjectIcon.DataPropertyName = "ObjectIcon";
            this.ObjectIcon.HeaderText = "";
            this.ObjectIcon.Name = "ObjectIcon";
            this.ObjectIcon.ReadOnly = true;
            this.ObjectIcon.Width = 40;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // ObjectName
            // 
            this.ObjectName.DataPropertyName = "ObjectName";
            this.ObjectName.HeaderText = "文件名";
            this.ObjectName.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.ObjectName.LinkColor = System.Drawing.Color.Black;
            this.ObjectName.Name = "ObjectName";
            this.ObjectName.ReadOnly = true;
            this.ObjectName.Width = 135;
            // 
            // ObjectSize
            // 
            this.ObjectSize.DataPropertyName = "ObjectSize";
            this.ObjectSize.HeaderText = "大小";
            this.ObjectSize.Name = "ObjectSize";
            this.ObjectSize.ReadOnly = true;
            this.ObjectSize.Width = 134;
            // 
            // ObjectLastModify
            // 
            this.ObjectLastModify.DataPropertyName = "ObjectLastModify";
            this.ObjectLastModify.HeaderText = "修改时间";
            this.ObjectLastModify.Name = "ObjectLastModify";
            this.ObjectLastModify.ReadOnly = true;
            this.ObjectLastModify.Width = 135;
            // 
            // ObjectType
            // 
            this.ObjectType.DataPropertyName = "ObjectType";
            this.ObjectType.HeaderText = "文件类型";
            this.ObjectType.Name = "ObjectType";
            this.ObjectType.ReadOnly = true;
            // 
            // ObjectExtension
            // 
            this.ObjectExtension.DataPropertyName = "ObjectExtension";
            this.ObjectExtension.HeaderText = "文件扩展名";
            this.ObjectExtension.Name = "ObjectExtension";
            this.ObjectExtension.ReadOnly = true;
            // 
            // ParentID
            // 
            this.ParentID.DataPropertyName = "ParentID";
            this.ParentID.HeaderText = "ParentID";
            this.ParentID.Name = "ParentID";
            this.ParentID.ReadOnly = true;
            this.ParentID.Visible = false;
            // 
            // ObjectFullPath
            // 
            this.ObjectFullPath.DataPropertyName = "ObjectFullPath";
            this.ObjectFullPath.HeaderText = "ObjectFullPath";
            this.ObjectFullPath.Name = "ObjectFullPath";
            this.ObjectFullPath.ReadOnly = true;
            this.ObjectFullPath.Visible = false;
            // 
            // ObjectEncryptedName
            // 
            this.ObjectEncryptedName.DataPropertyName = "ObjectEncryptedName";
            this.ObjectEncryptedName.HeaderText = "ObjectEncryptedName";
            this.ObjectEncryptedName.Name = "ObjectEncryptedName";
            this.ObjectEncryptedName.ReadOnly = true;
            this.ObjectEncryptedName.Visible = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.下载ToolStripMenuItem1,
            this.上传ToolStripMenuItem1,
            this.删除ToolStripMenuItem1,
            this.新建文件夹ToolStripMenuItem1,
            this.复制ToolStripMenuItem,
            this.剪切ToolStripMenuItem,
            this.粘贴ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(137, 158);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // 下载ToolStripMenuItem1
            // 
            this.下载ToolStripMenuItem1.Name = "下载ToolStripMenuItem1";
            this.下载ToolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.下载ToolStripMenuItem1.Text = "下载";
            // 
            // 上传ToolStripMenuItem1
            // 
            this.上传ToolStripMenuItem1.Name = "上传ToolStripMenuItem1";
            this.上传ToolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.上传ToolStripMenuItem1.Text = "上传";
            // 
            // 删除ToolStripMenuItem1
            // 
            this.删除ToolStripMenuItem1.Name = "删除ToolStripMenuItem1";
            this.删除ToolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.删除ToolStripMenuItem1.Text = "删除";
            // 
            // 新建文件夹ToolStripMenuItem1
            // 
            this.新建文件夹ToolStripMenuItem1.Name = "新建文件夹ToolStripMenuItem1";
            this.新建文件夹ToolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.新建文件夹ToolStripMenuItem1.Text = "新建文件夹";
            // 
            // 复制ToolStripMenuItem
            // 
            this.复制ToolStripMenuItem.Name = "复制ToolStripMenuItem";
            this.复制ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.复制ToolStripMenuItem.Text = "复制";
            // 
            // 剪切ToolStripMenuItem
            // 
            this.剪切ToolStripMenuItem.Name = "剪切ToolStripMenuItem";
            this.剪切ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.剪切ToolStripMenuItem.Text = "剪切";
            // 
            // 粘贴ToolStripMenuItem
            // 
            this.粘贴ToolStripMenuItem.Name = "粘贴ToolStripMenuItem";
            this.粘贴ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.粘贴ToolStripMenuItem.Text = "粘贴";
            // 
            // txtFullPath
            // 
            this.txtFullPath.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFullPath.Location = new System.Drawing.Point(0, 29);
            this.txtFullPath.Name = "txtFullPath";
            this.txtFullPath.ReadOnly = true;
            this.txtFullPath.Size = new System.Drawing.Size(583, 21);
            this.txtFullPath.TabIndex = 2;
            this.txtFullPath.Text = "/";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // tsmi_Close
            // 
            this.tsmi_Close.Name = "tsmi_Close";
            this.tsmi_Close.Size = new System.Drawing.Size(68, 21);
            this.tsmi_Close.Text = "关闭工具";
            this.tsmi_Close.Click += new System.EventHandler(this.tsmi_Close_Click);
            // 
            // EncrytFileForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 317);
            this.Controls.Add(this.txtFullPath);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "EncrytFileForm";
            this.Text = "文件加密工具";
            this.Load += new System.EventHandler(this.EncrytFileForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmi_Upload;
        private System.Windows.Forms.ToolStripMenuItem tsmi_Download;
        private System.Windows.Forms.ToolStripMenuItem tsmi_Delete;
        private System.Windows.Forms.ToolStripMenuItem tsmi_NewFolder;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 下载ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 上传ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 新建文件夹ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 复制ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 剪切ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 粘贴ToolStripMenuItem;
        private System.Windows.Forms.TextBox txtFullPath;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn MultSelect;
        private System.Windows.Forms.DataGridViewImageColumn ObjectIcon;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewLinkColumn ObjectName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ObjectSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn ObjectLastModify;
        private System.Windows.Forms.DataGridViewTextBoxColumn ObjectType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ObjectExtension;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ObjectFullPath;
        private System.Windows.Forms.DataGridViewTextBoxColumn ObjectEncryptedName;
        private System.Windows.Forms.ToolStripMenuItem tsmi_Close;
    }
}

