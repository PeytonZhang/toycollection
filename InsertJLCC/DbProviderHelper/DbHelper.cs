﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DbProviderHelper
{
    public class DbHelper
    {
        private DbProviderFactory providerFactory = null;

        static volatile DbHelper instance = null;
        static readonly object padlock = new object();

        private string connString;

        public string ConnString
        {
            get;
            private set;
        }

        private DbHelper(DbProviderType providerType, string connString)
        {
            this.ConnString = connString;
            providerFactory = ProviderFactory.getProviderFactory(providerType);
            if (providerFactory == null)
            {
                throw new ArgumentException("无法取得正确的管道！");
            }
        }

        public static DbHelper getDbFactory(DbProviderType providerType, string connString)
        {
            if (instance == null)
            {
                lock (padlock)
                {

                    if (instance == null)
                    {
                        instance = new DbHelper(providerType, connString);
                    }
                }
            }
            return instance;

        }

        #region 执行sql语句

        /// <summary>
        /// 根据传入的CommandType来确定执行sql还是存储过程
        /// </summary>
        /// <param name="commandType"></param>
        /// <param name="sql"></param>
        /// <param name="para"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(CommandType commandType, string sql, params DbParameter[] para)
        {
            using (DbConnection connection = providerFactory.CreateConnection())
            {
                DbCommand command = providerFactory.CreateCommand();
                connection.ConnectionString = this.ConnString;
                command.CommandText = sql;
                command.CommandType = commandType;
                command.Connection = connection;

                if (para != null && para.Length > 0)
                {
                    foreach (DbParameter parameter in para)
                    {
                        if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
                            (parameter.Value == null))
                        {
                            parameter.Value = DBNull.Value;
                        }
                        command.Parameters.Add(((ICloneable)parameter).Clone());
                    }
                }
                command.Connection.Open();
                return command.ExecuteNonQuery();

            }
        }
        /// <summary>
        /// 执行sql语句
        /// </summary>
        /// <param name="sql">查询sql</param>
        /// <param name="para">查询参数</param>
        /// <returns></returns>
        public int ExecuteNonQuery(string sql, params DbParameter[] para)
        {
            return ExecuteNonQuery(CommandType.Text, sql, para);
        }

        #endregion

        #region 执行sql查询，返回首行首列
        /// <summary>
        /// 执行sql或存储过程返回首行首列
        /// </summary>
        /// <param name="commandType"></param>
        /// <param name="sql"></param>
        /// <param name="para"></param>
        /// <returns></returns>
        public object ExecuteScaler(CommandType commandType, string sql, params DbParameter[] para)
        {
            using (DbConnection connection = providerFactory.CreateConnection())
            {
                DbCommand command = providerFactory.CreateCommand();
                connection.ConnectionString = this.ConnString;
                command.CommandText = sql;
                command.CommandType = commandType;
                command.Connection = connection;

                if (para != null && para.Length > 0)
                {
                    foreach (DbParameter parameter in para)
                    {
                        if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
                            (parameter.Value == null))
                        {
                            parameter.Value = DBNull.Value;
                        }
                        command.Parameters.Add(((ICloneable)parameter).Clone());
                    }
                }
                command.Connection.Open();
                return command.ExecuteScalar();

            }
        }

        /// <summary>
        /// 执行sql语句查询返回首行首列的值
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="para"></param>
        /// <returns></returns>
        public object ExecuteScaler(string sql, params DbParameter[] para)
        {
            return ExecuteScaler(CommandType.Text, sql, para);
        }

        #endregion

        #region 执行sql查询，返回DataTable
        /// <summary>
        /// 执行sql语句或存储过程查询返回结果集的DataTable
        /// </summary>
        /// <param name="commandType"></param>
        /// <param name="sql"></param>
        /// <param name="para"></param>
        /// <returns></returns>
        public DataTable getDataTable(CommandType commandType, string sql, params DbParameter[] para)
        {
            using (DbConnection connection = providerFactory.CreateConnection())
            {
                DbCommand command = providerFactory.CreateCommand();
                connection.ConnectionString = this.ConnString;
                command.CommandText = sql;
                command.CommandType = commandType;
                command.Connection = connection;

                if (para != null && para.Length > 0)
                {
                    foreach (DbParameter parameter in para)
                    {
                        if ((parameter.Direction == ParameterDirection.InputOutput || parameter.Direction == ParameterDirection.Input) &&
                            (parameter.Value == null))
                        {
                            parameter.Value = DBNull.Value;
                        }
                        command.Parameters.Add(((ICloneable)parameter).Clone());
                    }
                }
                using (DbDataAdapter adapter = providerFactory.CreateDataAdapter())
                {
                    adapter.SelectCommand = command;
                    DataTable data = new DataTable();
                    adapter.Fill(data);
                    return data;
                }

            }
        }
        /// <summary>
        /// 执行sql语句查询返回结果集的DataTable
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="para"></param>
        /// <returns></returns>
        public DataTable getDataTable(string sql, params DbParameter[] para)
        {
            return getDataTable(CommandType.Text, sql, para);
        }

        #endregion

        #region getDataSet

        /// <summary>
        /// 执行sql语句查询返回结果集的DataSet
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="para"></param>
        /// <returns></returns>
        public DataSet getDataSet(string sql, params DbParameter[] para)
        {
            DataTable dt = getDataTable(CommandType.Text, sql, para);
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            return ds;
        }

        /// <summary>
        /// 执行sql语句或存储过程查询返回结果集的DataSet
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="para"></param>
        /// <returns></returns>
        public DataSet getDataSet(CommandType commandType, string sql, params DbParameter[] para)
        {
            DataTable dt = getDataTable(commandType, sql, para);
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            return ds;
        }

        #endregion

        #region 面向对象的查询
        /// <summary>
        /// 根据主键获取对象
        /// </summary>
        /// <param name="commandType"></param>
        /// <param name="sql"></param>
        /// <param name="para"></param>
        /// <returns></returns>
        public T getObjectById<T>(string IdKey, decimal IdValue)
        {

            using (DbConnection connection = providerFactory.CreateConnection())
            {
                Type t = typeof(T);
                object list = Activator.CreateInstance(t);//获取当前类的实例，相当于object list = new ClassTest();
                string sqlWhere = "select * from " + t.Name + " where " + IdKey + " = :id";

                DbCommand command = providerFactory.CreateCommand();
                DbDataReader reader = null;
                connection.ConnectionString = this.ConnString;

                DbParameter para = (DbParameter)command.CreateParameter();
                para.ParameterName = ":id";
                para.Value = IdValue;

                command.CommandType = CommandType.Text;
                command.Connection = connection;
                command.CommandText = sqlWhere;
                command.Parameters.Add(para);
                command.Connection.Open();
                reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                PropertyInfo[] pi = t.GetProperties();
                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        foreach (PropertyInfo pi1 in pi)
                        {
                            if (pi1.Name.ToLower() == reader.GetName(i).ToLower() && pi1.PropertyType.FullName != "System.Byte[]")
                            {
                                pi1.SetValue(list, reader[i], null);
                            }
                            else if (pi1.Name.ToLower() == reader.GetName(i).ToLower() && pi1.PropertyType.FullName == "System.Byte[]")
                            {
                                object obj = reader[i] == DBNull.Value ? null : ((byte[])reader[i]);
                                pi1.SetValue(list, obj, null);
                            }
                        }
                    }
                }
                return (T)list;

            }

        }
        /// <summary>
        /// 执行sql语句查询返回结果集的DataReader
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="para"></param>
        /// <returns></returns>
        public IList<T> getObjectList<T>(string sqlWhere, params DbParameter[] para)
        {

            using (DbConnection connection = providerFactory.CreateConnection())
            {
                IList<T> tList = new List<T>();
                Type t = typeof(T);

                DbCommand command = providerFactory.CreateCommand();
                DbDataReader reader = null;
                connection.ConnectionString = this.ConnString;

                if (para != null && para.Length > 0)
                {
                    for (int i = 0; i < para.Length; i++)
                    {
                        command.Parameters.Add(para[i]);
                    }
                }

                command.CommandType = CommandType.Text;
                command.Connection = connection;
                command.CommandText = sqlWhere;
                command.Connection.Open();
                reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                PropertyInfo[] pi = t.GetProperties();
                while (reader.Read())
                {
                    object list = Activator.CreateInstance(t);//获取当前类的实例，相当于object list = new ClassTest();
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        foreach (PropertyInfo pi1 in pi)
                        {
                            if (pi1.Name.ToLower() == reader.GetName(i).ToLower() && pi1.PropertyType.FullName != "System.Byte[]")
                            {
                                pi1.SetValue(list, reader[i], null);
                            }
                        }
                    }
                    tList.Add((T)list);
                }

                return tList;
            }

        }

        /// <summary>
        /// 执行sql语句查询返回结果集的DataReader
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="para"></param>
        /// <returns></returns>
        public IList<T> getObjectByFKId<T>(string IdKey, decimal IdValue)
        {

            using (DbConnection connection = providerFactory.CreateConnection())
            {
                IList<T> tList = new List<T>();
                Type t = typeof(T);
                string sqlWhere = "select * from " + t.Name + " where " + IdKey + " = :id";
                DbCommand command = providerFactory.CreateCommand();
                DbDataReader reader = null;
                connection.ConnectionString = this.ConnString;

                command.CommandType = CommandType.Text;
                command.Connection = connection;
                command.CommandText = sqlWhere;
                command.Connection.Open();
                reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                PropertyInfo[] pi = t.GetProperties();
                while (reader.Read())
                {
                    object list = Activator.CreateInstance(t);//获取当前类的实例，相当于object list = new ClassTest();
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        foreach (PropertyInfo pi1 in pi)
                        {
                            if (pi1.Name.ToLower() == reader.GetName(i).ToLower() && pi1.PropertyType.FullName != "System.Byte[]")
                            {
                                pi1.SetValue(list, reader[i], null);
                            }
                            else if (pi1.Name.ToLower() == reader.GetName(i).ToLower() && pi1.PropertyType.FullName == "System.Byte[]")
                            {
                                object obj = reader[i] == DBNull.Value ? null : ((byte[])reader[i]);
                                pi1.SetValue(list, obj, null);
                            }
                        }
                    }
                    tList.Add((T)list);
                }

                return tList;
            }

        }
        #endregion

    }
}
