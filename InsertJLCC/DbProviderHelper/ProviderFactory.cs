﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DbProviderHelper
{
    public class ProviderFactory
    {
        private readonly static Dictionary<DbProviderType, string> providerInvariantNames = new Dictionary<DbProviderType, string>();
        private readonly static Dictionary<DbProviderType, DbProviderFactory> providerFactoies = new Dictionary<DbProviderType, DbProviderFactory>(20);

        static ProviderFactory()
        {
            providerInvariantNames.Add(DbProviderType.SqlServer, "System.Data.SqlClient");
            providerInvariantNames.Add(DbProviderType.OleDb, "System.Data.OleDb");
            providerInvariantNames.Add(DbProviderType.ODBC, "System.Data.ODBC");
            //providerInvariantNames.Add(DbProviderType.Oracle, "Oracle.DataAccess.Client");
            providerInvariantNames.Add(DbProviderType.Oracle, "System.Data.OracleClient");
            providerInvariantNames.Add(DbProviderType.MySql, "MySql.Data.MySqlClient");
            providerInvariantNames.Add(DbProviderType.SQLite, "System.Data.SQLite");
            providerInvariantNames.Add(DbProviderType.Firebird, "FirebirdSql.Data.Firebird");
            providerInvariantNames.Add(DbProviderType.PostgreSql, "Npgsql");
            providerInvariantNames.Add(DbProviderType.DB2, "IBM.Data.DB2.iSeries");
            providerInvariantNames.Add(DbProviderType.Informix, "IBM.Data.Informix");
            providerInvariantNames.Add(DbProviderType.SqlServerCe, "System.Data.SqlServerCe");
        }

        public static DbProviderFactory getProviderFactory(DbProviderType providerType)
        {
            if (!providerFactoies.ContainsKey(providerType))
            {
                providerFactoies.Add(providerType, importProvider(providerType));
            }
            return providerFactoies[providerType];
        }

        private static DbProviderFactory importProvider(DbProviderType providerType)
        {
            return DbProviderFactories.GetFactory(providerInvariantNames[providerType]);
        }
    }

    public enum DbProviderType : byte
    {
        /// <summary>
        /// 微软 SqlServer 数据库
        /// </summary>
        SqlServer,
        /// <summary>
        /// 开源 MySql数据库
        /// </summary>
        MySql,
        /// <summary>
        /// 嵌入式轻型数据库 SQLite
        /// </summary>
        SQLite,
        /// <summary>
        /// 甲骨文 Oracle
        /// </summary>
        Oracle,
        /// <summary>
        /// 开放数据库互连
        /// </summary>
        ODBC,
        /// <summary>
        /// 面向不同的数据源的低级应用程序接口
        /// </summary>
        OleDb,
        /// <summary>
        /// 跨平台的关系数据库系统 Firebird
        /// </summary>
        Firebird,
        /// <summary>
        ///加州大学伯克利分校计算机系开发的关系型数据库 PostgreSql
        /// </summary>
        PostgreSql,
        /// <summary>
        /// IBM出口的一系列关系型数据库管理系统 DB2
        /// </summary>
        DB2,
        /// <summary>
        /// IBM公司出品的关系数据库管理系统（RDBMS）家族  Informix
        /// </summary>
        Informix,
        /// <summary>
        /// 微软推出的一个适用于嵌入到移动应用的精简数据库产品 SqlServerCe
        /// </summary>
        SqlServerCe
    }
}
