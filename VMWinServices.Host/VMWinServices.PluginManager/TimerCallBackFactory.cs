﻿/* ==============================================================================
 * Description：TimerCallBackFactory  
 * Author：Peytonzhang
 * Created：5/17/2016 10:36:27 AM
 * ==============================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMWinServices.DomainInterface;
using VMWinServices.PlugInInterface;
using VMWinServices.PluginManager.ConfigurationFile;
using VMWinServices.PluginManager.PluginConfig;

namespace VMWinServices.PluginManager
{
    public static class TimerCallBackFactory
    {
        private static readonly System.Threading.TimerCallback CheckAllJobTimer = new System.Threading.TimerCallback(CheckAllPlugInJobAlready);
        internal static bool _isUnloadAppDomain = false;
        private static System.Threading.Timer _checkTimer;

        internal static void CheckPlugInUpgradState(object state)
        {
            Logger.Logger.WriteLog("Start check plugin upgrade status");
            Dictionary<string, object> timerParamDict = state as Dictionary<string, object>;
            IEnumerable<IExecute> exports;
            string CopyPath = String.Empty;
            string AzureTableAccountInfo = String.Empty;
            try
            {
                exports = timerParamDict["IExecute"] as IEnumerable<IExecute>;
                CopyPath = timerParamDict["CopyPath"].ToString();
                AzureTableAccountInfo = timerParamDict["AzureTableAccountInfo"].ToString();
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }


            var upgrade = new PlugInUpgrade();

            if (exports == null)
                return;

            List<PlugInConfigModel> plugInConfigList = upgrade.GetNeedUpgradePlugIn(AzureTableAccountInfo);
            if (plugInConfigList != null)
            {
                //There is no need update all plugins
                if (plugInConfigList.Count == 0) return;


                upgrade.DownLoadPlugIn(plugInConfigList, CopyPath);

                List<PlugInConfigModel> pluginConfigFileList= PlugInConfigReader.GetAllConfigurition(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "MultPlugInConfig.xml")); //local plugin list

                string[] localPluginFolderNameList = System.IO.Directory.GetDirectories(CopyPath);

                foreach (string localPluginFolderName in localPluginFolderNameList)
                {
                    string FolderName = localPluginFolderName.Substring(localPluginFolderName.LastIndexOf(@"\") + 1);
                    if(!pluginConfigFileList.Select(s => s.PlugInName).Contains(FolderName))
                    {
                        upgrade.CleanUpFolder(localPluginFolderName);
                    }
                    
                }
            }
            else
            {
                foreach(string dirString in System.IO.Directory.GetDirectories(CopyPath))
                {
                    upgrade.CleanUpFolder(dirString);
                }
            }
            
            try
            {
                exports.ToList().ForEach(e =>
                {
                    e.DoNotCreateNewJob(new ExecuteArgs() { IsNotCreateNewJob = true });
                });
            }
            catch (Exception ex)
            {
                Logger.Logger.ErrorLog("Call DoNotCreateNewJob error", ex);
            }


            //Check all job is or not complated

            long timerSplit = long.Parse(ConfigUnit.getConfigItem("CheckPlugAllJobIsComplated"));
            _checkTimer = new System.Threading.Timer(CheckAllJobTimer, exports, 0, timerSplit);
        }
        /// <summary>
        /// Check all job is or not complated
        /// </summary>
        /// <param name="state"></param>
        internal static void CheckAllPlugInJobAlready(object state)
        {
            Logger.Logger.WriteLog("Start check all job running status");

            IEnumerable<IExecute> exports = state as IEnumerable<IExecute>;
            if (exports == null)
            {
                _checkTimer.Dispose();
                return;
            }

            foreach (var ex in exports.ToList())
            {
                if (!ex.IsComplatedAllJob())
                    return;
                else
                    continue;
            }
            Logger.Logger.WriteLog("Set the state, Tell host can unload this appDomain");
            _isUnloadAppDomain = true;
            try
            {
                _checkTimer.Dispose();
            }catch(Exception ex)
            {
                Logger.Logger.ErrorLog("Dispose timer error", ex);
            }
            Logger.Logger.WriteLog("Dispose timer success");
        }
    }
}
