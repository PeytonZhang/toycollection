﻿/* ==============================================================================
 * Description：AbstractManager  
 * Author：peytonzhang
 * Created：6/16/2016 1:32:31 PM
 * ==============================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Registration;
using VMWinServices.DomainInterface;
using VMWinServices.PlugInInterface;
using System.IO;
using VMWinServices.PluginManager.ConfigurationFile;

namespace VMWinServices.PluginManager
{
    public abstract class AbstractManager : MarshalByRefObject, IMultAppDomain
    {
        protected IEnumerable<IExecute> _exports;
        private readonly System.Threading.TimerCallback _plugInUpgradTimer = new System.Threading.TimerCallback(TimerCallBackFactory.CheckPlugInUpgradState);
        private System.Threading.Timer _timer;

        public void DoWorkInShadowCopiedDomain(Dictionary<string, object> appDomainParamDict)
        {
            try
            {
                string CopyPath = appDomainParamDict["CopyPath"].ToString();

                //
                var regBuilder = new RegistrationBuilder();
                regBuilder.ForTypesDerivedFrom<IExecute>().Export<IExecute>();

                var catalog = new AggregateCatalog();
                //catalog.Catalogs.Add(new AssemblyCatalog(typeof(Runner).Assembly, regBuilder));

                try
                {
                    var directoryArray = Directory.GetDirectories(CopyPath);
                    foreach (string directorypath in directoryArray)
                    {
                        DirectoryCatalog directoryCatalog = new DirectoryCatalog(directorypath, "VMWinServices.PlugIn.*.dll", regBuilder);
                        catalog.Catalogs.Add(directoryCatalog);
                    }

                    CompositionContainer container = new CompositionContainer(catalog);
                    container.ComposeExportedValue(container);
                    // Get our _exports available to the rest of Program.
                    _exports = container.GetExportedValues<IExecute>();

                }
                catch (System.Reflection.ReflectionTypeLoadException tex)
                {
                    if (tex.LoaderExceptions.Length > 0)
                    {
                        foreach (Exception loadException in tex.LoaderExceptions)
                        {
                            Logger.Logger.ErrorLog("Load plugin assembly type error", loadException);
                        }
                    }
                    else
                    {
                        Logger.Logger.ErrorLog("Load plugin assembly type error", tex);
                    }
                }

                try
                {
                    appDomainParamDict.Add("IExecute", _exports);
                    long timerSplit = long.Parse(ConfigUnit.getConfigItem("CheckPlugInWhetherUpgradeTime"));
                    _timer = new Timer(_plugInUpgradTimer, appDomainParamDict, 0, timerSplit);
                }
                catch (TimeoutException ex)
                {
                    Logger.Logger.ErrorLog("Timer controle timerout", ex);
                }
            }
            catch (Exception exception)
            {
                Logger.Logger.ErrorLog("DoWorkInShadowCopiedDomain Error", exception);
            }


        }

        public abstract void RunTask(Dictionary<string, object> appDomainParamDict);

        public bool IsReloadAppDomain()
        {
            return TimerCallBackFactory._isUnloadAppDomain;
        }
    }
}
