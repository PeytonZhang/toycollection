﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMWinServices.PlugInInterface;
using VMWinServices.PluginManager.ConfigurationFile;

namespace VMWinServices.PluginManager
{
    public class DefaultManager : AbstractManager
    {
        public DefaultManager()
        {
            ConfigUnit.config = new LocalConfiguration(typeof(DefaultManager));
        }

        public override void RunTask(Dictionary<string, object> appDomainParamDict)
        {
            List<System.Threading.Thread> threadManageList = new List<System.Threading.Thread>();
            System.Threading.Thread thread;

            _exports.ToList().ForEach(e =>
            {
                try
                {
                    thread = new System.Threading.Thread(StartThread);
                    thread.Start(e);
                    threadManageList.Add(thread);
                }
                catch (Exception ex)
                {
                    throw ex.InnerException;
                }
            });
            Logger.Logger.WriteLog("All plugIn was loaded");

        }

        private static void StartThread(object obj)
        {
            try
            {
                var execute = obj as IExecute;

                var appDomainArgs = new ExecuteArgs();

                if (execute != null)
                {
                    execute.OnStart(appDomainArgs);
                    Logger.Logger.DebugLog(execute.GetType().Name + " plugIn was loaded", null);
                }
            }catch(Exception ex)
            {
                throw ex.InnerException;
            }
            
        }
    }
}
