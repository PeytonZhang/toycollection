﻿/* ==============================================================================
 * Description：PlugInConfigModel  
 * Author：Peytonzhang
 * Created：5/17/2016 10:32:02 AM
 * ==============================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMWinServices.PluginManager.PluginConfig
{
    public class PlugInConfigModel
    {
        public string PlugInName { get; set; }

        public string PlugInVersion { get; set; }

        public string PlugInDesc { get; set; }

        public string PlugInContainer { get; set; }

        public string PlugInBlobName { get; set; }
    }
}
