﻿/* ==============================================================================
 * Description：PlugInConfigReader  
 * Author：Peytonzhang
 * Created：5/17/2016 10:31:22 AM
 * ==============================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace VMWinServices.PluginManager.PluginConfig
{
    public class PlugInConfigReader
    {
        private object obj = new object();


        public static List<PlugInConfigModel> GetAllConfigurition(string xmlpath)
        {
            XDocument xDoc;
            try
            {
                xDoc = XDocument.Load(xmlpath);
            }catch(Exception ex)
            {
                throw ex.InnerException;
            }

            List<PlugInConfigModel> configModelList = GetAllConfig(xDoc);

            return configModelList;
        }

        private static List<PlugInConfigModel> GetAllConfig(XDocument xDoc)
        {
            List<PlugInConfigModel> configModelList = new List<PlugInConfigModel>();
            IEnumerable<XElement> xele = xDoc.Root.Elements();
            foreach (var xelement in xele.ToList())
            {
                var configModel = new PlugInConfigModel
                {
                    PlugInName = xelement.Attribute("Name").Value
                };
                var childxele = xelement.Elements().ToList();

                foreach (var nodexele in childxele)
                {
                    switch (nodexele.Name.LocalName)
                    {
                        case "PlugInVersion":
                            configModel.PlugInVersion = nodexele.Value;
                            break;
                        case "Descript":
                            configModel.PlugInDesc = nodexele.Value;
                            break;
                        case "PlugInContainer":
                            configModel.PlugInContainer = nodexele.Value;
                            break;
                        case "PlugInBlobName":
                            configModel.PlugInBlobName = nodexele.Value;
                            break;
                    }
                }
                configModelList.Add(configModel);
            }
            return configModelList;
        }

        public static PlugInConfigModel GetConfigurationByPlugInName(string xmlPath, string PlugInName)
        {
            var configModel = new PlugInConfigModel();
            XDocument xDoc;
            try
            {
                xDoc = XDocument.Load(xmlPath);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
            if (xDoc.Root == null) return null;

            List<PlugInConfigModel> configModelList = GetAllConfig(xDoc);
            if (configModelList.Where(s => s.PlugInName == PlugInName).ToList<PlugInConfigModel>().Count > 0)
                configModel = configModelList.Where(s => s.PlugInName == PlugInName).ToList<PlugInConfigModel>().First();
            return configModel;
        }

        public static List<PlugInConfigModel> GetAllConfiguritionForXML(System.IO.Stream xml)
        {
            XDocument xDoc;
            try
            {
                xDoc = XDocument.Load(xml);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

            List<PlugInConfigModel> configModelList = GetAllConfig(xDoc);

            return configModelList;
        }

        public static PlugInConfigModel GetConfigurationByPlugInNameForXML(System.IO.Stream XML, string PlugInName)
        {
            var configModel = new PlugInConfigModel();
            XDocument xDoc;
            try
            {
                xDoc = XDocument.Load(XML);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
            if (xDoc.Root == null) return null;
            List<PlugInConfigModel> configModelList = GetAllConfig(xDoc);
            if (configModelList.Where(s => s.PlugInName == PlugInName).ToList<PlugInConfigModel>().Count > 0)
                configModel = configModelList.Where(s => s.PlugInName == PlugInName).ToList<PlugInConfigModel>().First();
            return configModel;
        }

        public static void ReplaceElementValueByPluginName(string xmlPath, List<PlugInConfigModel> pluginConfigList)
        {
            XDocument xDoc;
            try
            {
                xDoc = XDocument.Load(xmlPath);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

            if (xDoc.Root == null) return;
            List<XElement> xele = xDoc.Root.Elements().ToList<XElement>();
            foreach (XElement configModel in ConvertListToXElement(pluginConfigList))
            {
                if (xele.Where(w => w.Attribute("Name").Value == configModel.Attribute("Name").Value).ToList().Count > 0)
                {
                    XElement firestXElement = xele.Where(w => w.Attribute("Name").Value == configModel.Attribute("Name").Value).ToList().FirstOrDefault<XElement>();
                    xele.Remove(firestXElement);
                    xele.Add(configModel);
                }
                else
                {
                    xele.Add(configModel);
                }
            }
            xDoc.Root.RemoveAll();
            foreach (XElement pluginNode in xele)
            {
                xDoc.Root.Add(pluginNode);
            }
            
            try
            {
                xDoc.Save(xmlPath);
            }
            catch (System.IO.FileNotFoundException ex)
            {
                throw ex.InnerException;
            }
            catch (System.Xml.XmlException ex)
            {
                throw ex.InnerException;
            }

        }

        private static List<XElement> ConvertListToXElement(List<PlugInConfigModel> pluginConfigList)
        {
            List<XElement> pluginXnodes = new List<XElement>();
            foreach(PlugInConfigModel plugin in pluginConfigList)
            {
                XElement pluginXel = new XElement("PlugIn",
                new XElement("PlugInVersion", plugin.PlugInVersion),
                new XElement("Descript", plugin.PlugInDesc),
                new XElement("PlugInContainer", plugin.PlugInContainer),
                new XElement("PlugInBlobName", plugin.PlugInBlobName)
                );
                pluginXel.SetAttributeValue("Name", plugin.PlugInName);
                pluginXnodes.Add(pluginXel);
            }

            return pluginXnodes;
        }

        public static void ReplaceElementValueToXml(string xmlFilePath, List<PlugInConfigModel> pluginConfigList)
        {
            List<XElement> pluginXelements = ConvertListToXElement(pluginConfigList);
            XDocument xDoc;
            try
            {
                xDoc = XDocument.Load(xmlFilePath);
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
            xDoc.Root.RemoveAll();
            foreach (XElement pluginNode in pluginXelements)
            {
                xDoc.Root.Add(pluginNode);
            }
            xDoc.Save(xmlFilePath);
        }
    }
}
