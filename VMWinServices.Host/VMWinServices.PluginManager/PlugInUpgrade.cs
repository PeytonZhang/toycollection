﻿/* ==============================================================================
 * Description：PlugInUpgrade  
 * Author：Peytonzhang
 * Created：5/17/2016 10:30:15 AM
 * ==============================================================================*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMWinServices.AzureBlobHelper;
using VMWinServices.AzureTableHelper;
using VMWinServices.AzureTableHelper.DbFactory;
using VMWinServices.AzureTableHelper.Model;
using VMWinServices.DomainInterface;
using VMWinServices.PluginManager.ConfigurationFile;
using VMWinServices.PluginManager.PluginConfig;

namespace VMWinServices.PluginManager
{
    internal class PlugInUpgrade
    {
        private string blobAccountKey = String.Empty;
        /// <summary>
        /// 1) If all plugin not change return null
        /// 2) If the instance status is Disbaled return list count is zero
        /// 3) If plugin was changed return all changed object list
        /// </summary>
        /// <param name="AzureTableAccountInfo"></param>
        /// <returns></returns>
        internal List<PlugInConfigModel> GetNeedUpgradePlugIn(string AzureTableAccountInfo)
        {
            var resultplugInList = new List<PlugInConfigModel>();
            //Query Table Record and Compare Local Configuration with Table Record
            var UpgradeplugInList = getPlugInConfigFromAzureTable(AzureTableAccountInfo);
            if(UpgradeplugInList.Count == 0)
            {
                //clean up local config file
                CleanUpVersionFile();
                return null;
            }
            else
            {
                var plugInConfigList = PlugInConfigReader.GetAllConfigurition(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "MultPlugInConfig.xml")); //local plugin list
                foreach (PlugInConfigModel upgradePlugIn in UpgradeplugInList)
                {
                    if (!plugInConfigList.Select(s => s.PlugInName).Contains(upgradePlugIn.PlugInName))
                    {
                        //If plugin do not in local, The plugin is need created in local
                        resultplugInList.Add(upgradePlugIn);
                    }
                    else
                    {
                        //If plugIn already in local(upgrade plugin), Compare plugin version.
                        //When plugin version is different, Need upgrade local plugin code.
                        List<PlugInConfigModel> containerPlugIn = plugInConfigList.Where(w => w.PlugInName == upgradePlugIn.PlugInName).ToList<PlugInConfigModel>();
                        if (containerPlugIn != null && containerPlugIn.Count > 0)
                        {
                            PlugInConfigModel currentPlugIn = containerPlugIn.First<PlugInConfigModel>();
                            if (currentPlugIn.PlugInVersion != upgradePlugIn.PlugInVersion || currentPlugIn.PlugInBlobName != upgradePlugIn.PlugInBlobName)
                                resultplugInList.Add(upgradePlugIn);
                        }
                    }
                }
                if (resultplugInList.Count > 0 || plugInConfigList.Count != UpgradeplugInList.Count)
                    ModifyVersionFile(UpgradeplugInList);
            }

            return resultplugInList;
        }

        private List<PlugInConfigModel> getPlugInConfigFromAzureTable(string AzureTableAccountInfo)
        {
            List<PlugInConfigModel> UpgradeplugInList;

            InstanceTableOperate instanceOperate = new InstanceTableOperate("InstanceConfig", AzureTableAccountInfo);

            string hostName = getHostName();
            Instance instanceModel = instanceOperate.getObjectListByInstanceName(hostName);

            if (instanceModel.InstanceStatus == InstanceStatusEnum.Enable.ToString())
            {
                if (String.IsNullOrWhiteSpace(instanceModel.DataCenterConfigName))
                    throw new Exception("DataCenterConfig should be not empty!");

                DataCenterTableOperate datacenterOperate = new DataCenterTableOperate("DataCenter", AzureTableAccountInfo);
                DataCenter datacenterModel = datacenterOperate.getObjectListByDataCenterConfigName(instanceModel.DataCenterConfigName);
                blobAccountKey = datacenterModel.BlobStorageAccount;
                Stream stream = new MemoryStream(Encoding.UTF8.GetBytes(datacenterModel.PlugInConfigXml));
                UpgradeplugInList = PlugInConfigReader.GetAllConfiguritionForXML(stream);
            }
            else
            {
                UpgradeplugInList = new List<PlugInConfigModel>();
            }
            return UpgradeplugInList;

        }

        internal void DownLoadPlugIn(List<PlugInConfigModel> plugInConfigList, string CopyPath)
        {
            if (plugInConfigList == null) return;
            foreach (PlugInConfigModel configModel in plugInConfigList)
            {
                
                string pluginpath = Path.Combine(CopyPath, configModel.PlugInName);

                //Get all folder in appDomain root directory
                //the all folder is plugin directory, and use pluginName as the name of the folder.

                if (Directory.Exists(pluginpath))
                {
                    CleanUpFolder(pluginpath);
                    Directory.CreateDirectory(pluginpath);
                }
                else
                {
                    Directory.CreateDirectory(pluginpath);
                }

                //DownLoad plugin assambles in azure storage
                string plugInTempFolder = string.IsNullOrWhiteSpace(ConfigUnit.getConfigItem("plugInTempFolder")) ? Path.Combine(CopyPath, "tempFolder") : ConfigUnit.getConfigItem("plugInTempFolder");
                
                Logger.Logger.WriteLog("Start download plugin [" + configModel.PlugInName + "] to " + plugInTempFolder);

                DownLoadBlobs downloadblob = new DownLoadBlobs(plugInTempFolder);
                string unzipfolder = downloadblob.DownloadToLocalFile(configModel.PlugInContainer, configModel.PlugInBlobName, blobAccountKey);
                
                Logger.Logger.WriteLog("download plugin [" + configModel.PlugInName + "] success, Path is " + unzipfolder);

                if (String.IsNullOrWhiteSpace(unzipfolder))
                {
                    configModel.PlugInVersion = "downloaderror";
                    ModifyVersionFileByPluginName(new List<PlugInConfigModel> { configModel });
                }
                else
                {
                    MoveFileToPlugInFolder(unzipfolder, pluginpath);
                }
                

            }
        }

        internal void CleanUpVersionFile()
        {
            List<PlugInConfigModel> configModelList = new List<PlugInConfigModel>();
            ModifyVersionFile(configModelList);
        }

        internal void ModifyVersionFile(List<PlugInConfigModel> pluginConfigModel)
        {
            PlugInConfigReader.ReplaceElementValueToXml(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "MultPlugInConfig.xml"), pluginConfigModel);
        }

        internal void ModifyVersionFileByPluginName(List<PlugInConfigModel> pluginConfigModel)
        {
            PlugInConfigReader.ReplaceElementValueByPluginName(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "MultPlugInConfig.xml"), pluginConfigModel);
        }

        private void MoveFileToPlugInFolder(string sourceFolder, string targetFolder)
        {
            try
            {
                string[] dirArray = Directory.GetDirectories(sourceFolder);
                if (dirArray.Length > 0)
                {
                    foreach (string folderPath in dirArray)
                    {
                        string targetPath = System.IO.Path.Combine(targetFolder, folderPath.Substring(folderPath.LastIndexOf(@"\") + 1));
                        MoveFileToPlugInFolder(folderPath, targetPath);
                    }
                }

                string[] fileArray = System.IO.Directory.GetFiles(sourceFolder);
                foreach (string sourcefilePath in fileArray)
                {
                    if (!Directory.Exists(targetFolder))
                        Directory.CreateDirectory(targetFolder);
                    string targetfilePath = System.IO.Path.Combine(targetFolder, sourcefilePath.Substring(sourcefilePath.LastIndexOf(@"\") + 1));
                    File.Move(sourcefilePath, targetfilePath);
                }
                Directory.Delete(sourceFolder);
            }catch(IOException ex)
            {
                Logger.Logger.ErrorLog("Move file error", ex.InnerException);
            }
            
        }

        internal void CleanUpFolder(string FolderPath)
        {
            try
            {
                string[] dirArray = Directory.GetDirectories(FolderPath);
                if (dirArray.Length > 0)
                {
                    foreach (string folder in dirArray)
                    {
                        CleanUpFolder(folder);
                    }
                }

                string[] fileArray = Directory.GetFiles(FolderPath);
                if (fileArray.Length > 0)
                {
                    foreach (string file in fileArray)
                    {
                        File.Delete(file);
                    }
                    Directory.Delete(FolderPath);
                }
                else
                {
                    Directory.Delete(FolderPath);
                }
            }catch(Exception ex)
            {
                Logger.Logger.ErrorLog("Cleanup file error", ex.InnerException);
            }
            
        }

        private string getHostName()
        {
            string hostName = System.Net.Dns.GetHostName();

            return hostName;
        }
    }
}
