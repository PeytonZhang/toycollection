﻿/* ==============================================================================
 * Description：ConfigUnit  
 * Author：peytonzhang
 * Created：5/18/2016 7:17:35 PM
 * ==============================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMWinServices.DomainInterface;

namespace VMWinServices.PluginManager.ConfigurationFile
{
    public class ConfigUnit
    {
        public static IConfiguration config {get;set;}

        public static string getConfigItem(string key)
        {
            return config.getConfigItem(key);
        }
    }
}
