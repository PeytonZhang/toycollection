﻿/* ==============================================================================
 * Description：LocalConfiguration  
 * Author：peytonzhang
 * Created：5/18/2016 7:20:23 PM
 * ==============================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMWinServices.DomainInterface;

namespace VMWinServices.PluginManager.ConfigurationFile
{
    public class LocalConfiguration : IConfiguration
    {
        private readonly string _assemblePath;
        public LocalConfiguration(Type t)
        {
            _assemblePath = t.Assembly.CodeBase.Replace("file:///", "");
        }
        public string getConfigItem(string key)
        {
            System.Configuration.Configuration cfg = System.Configuration.ConfigurationManager.OpenExeConfiguration(_assemblePath);
            string ret;
            try
            {
                ret = cfg.AppSettings.Settings[key].Value;
            }
            catch (Exception)
            {
                return null;
            }

            return ret;
        }
    }
}
