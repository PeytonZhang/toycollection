﻿/* ==============================================================================
 * Description：RunServices  
 * Author：peytonzhang
 * Created：5/23/2016 3:37:27 PM
 * ==============================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMWinServices.AzureTableHelper;

namespace VMWinServices.AppDomainManager
{
    public class RunServices
    {
        public RunServices()
        {

        }

        public void Start()
        {
            RegisterInstanceInfoToAzureTable();

            DomainManage domainManage = new DomainManage();
            domainManage.ManageDomain();
        }

        public void Stop()
        {

        }

        private void RegisterInstanceInfoToAzureTable()
        {
            VMWinServices.AzureTableHelper.DbFactory.InstanceTableOperate operate
                = new VMWinServices.AzureTableHelper.DbFactory.InstanceTableOperate("InstanceConfig", Config.AzureTableAccountInfo);
            VMWinServices.AzureTableHelper.Model.Instance instance = new VMWinServices.AzureTableHelper.Model.Instance();
            //instance.DataCenterConfigName = "S1Config";
            instance.InstanceStatus = InstanceStatusEnum.Disabled.ToString();
            instance.InstanceName = getHostName();
            VMWinServices.AzureTableHelper.Model.Instance alreadyInstance = operate.getObjectListByInstanceName(instance.InstanceName);
            if (alreadyInstance == null)
                operate.SaveOrUpdate(instance);
        }

        private string getHostName()
        {
            string hostName = System.Net.Dns.GetHostName();

            return hostName;
        }
    }

}
