﻿/* ==============================================================================
 * Description：Config  
 * Author：peytonzhang
 * Created：5/23/2016 3:40:47 PM
 * ==============================================================================*/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMWinServices.AppDomainManager
{
    public class Config
    {
        public static string WinServicesName
        {
            get
            {
                if (ConfigurationManager.AppSettings["WinServicesName"] != null && ConfigurationManager.AppSettings["WinServicesName"].ToString() != "")
                    return ConfigurationManager.AppSettings["WinServicesName"].ToString();
                else
                    return "";
            }
        }

        public static string ServiceName
        {
            get
            {
                if (ConfigurationManager.AppSettings["WinServicesName"] != null && ConfigurationManager.AppSettings["WinServicesName"].ToString() != "")
                    return ConfigurationManager.AppSettings["WinServicesName"].ToString();
                else
                    return "";
            }
        }

        public static string Description
        {
            get
            {
                if (ConfigurationManager.AppSettings["description"] != null && ConfigurationManager.AppSettings["description"].ToString() != "")
                    return ConfigurationManager.AppSettings["description"].ToString();
                else
                    return "";
            }
        }

        public static long RunTime
        {
            get
            {
                if (ConfigurationManager.AppSettings["RunTime"] != null && ConfigurationManager.AppSettings["RunTime"].ToString() != "")
                    return long.Parse(ConfigurationManager.AppSettings["RunTime"].ToString());
                else
                    return 10000;
            }
        }

        public static string AzureTableAccountInfo
        {
            get
            {
                if (ConfigurationManager.AppSettings["AzureTableAccountInfo"] != null && ConfigurationManager.AppSettings["AzureTableAccountInfo"].ToString() != "")
                    return ConfigurationManager.AppSettings["AzureTableAccountInfo"].ToString();
                else
                    return "";
            }
        }
    }
}
