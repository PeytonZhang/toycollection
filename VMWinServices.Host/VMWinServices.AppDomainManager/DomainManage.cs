﻿/* ==============================================================================
 * Description：DomainManage  
 * Author：Peytonzhang
 * Created：5/16/2016 6:20:50 PM
 * ==============================================================================*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VMWinServices.DomainInterface;

namespace VMWinServices.AppDomainManager
{
    internal class DomainManage
    {
        private static Dictionary<IMultAppDomain, AppDomain> _domainDict;
        private readonly AppDomainConfigFactory _factory;

        private System.Timers.Timer _timer;
        public DomainManage()
        {
            _factory = new AppDomainConfigFactory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "MultAppDomainConfig.xml"));
            _domainDict = new Dictionary<IMultAppDomain, AppDomain>();
        }

        public void ManageDomain()
        {
            try
            {
                StartAppDomain();
                _timer = new System.Timers.Timer(Config.RunTime);
                _timer.Elapsed += _timer_Elapsed;
                _timer.Start();
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
            
        }

        /// <summary>
        /// timer func. check all appDomain whether or not reload
        /// </summary>
        void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Dictionary<IMultAppDomain, AppDomain> tempDict = new Dictionary<IMultAppDomain, AppDomain>();
            foreach (IMultAppDomain domainRunner in _domainDict.Keys)
            {
                if (domainRunner.IsReloadAppDomain())
                {
                    try
                    {
                        string appDomainFriendlyName = _domainDict[domainRunner].FriendlyName;
                        Logger.Logger.DebugLog("Start unload appDomain,appDomain friendly name is " + appDomainFriendlyName, null);
                        AppDomain.Unload(_domainDict[domainRunner]);
                        //appList.Add(domainRunner);

                        AppDomainConfigModel configModel = _factory.CreateConfig(appDomainFriendlyName);
                        Logger.Logger.DebugLog("Start load appDomain", null);
                        var aDomain = CreateAppDomain(configModel);
                        var runner = CreateObject(aDomain, configModel);

                        //AppDomain assembly parameters
                        Dictionary<string, object> appDomainParamDict = new Dictionary<string, object>();
                        appDomainParamDict.Add("CopyPath", configModel.CopyPath);
                        appDomainParamDict.Add("AzureTableAccountInfo", Config.AzureTableAccountInfo);

                        runner.DoWorkInShadowCopiedDomain(appDomainParamDict);
                        runner.RunTask(appDomainParamDict);
                        tempDict.Add(runner, aDomain);
                        Logger.Logger.DebugLog("Already load appDomain", null);
                    }
                    catch (AppDomainUnloadedException ex)
                    {
                        Logger.Logger.ErrorLog("Reload appdomain error", ex.InnerException);
                    }

                }
                else
                {
                    tempDict.Add(domainRunner, _domainDict[domainRunner]);
                }
            }
            _domainDict.Clear();

            foreach (IMultAppDomain domainRunner in tempDict.Keys)
            {
                _domainDict.Add(domainRunner, tempDict[domainRunner]);
            }
        }

        private void StartAppDomain()
        {
            var configModelList = _factory.CreateAllConfig();

            foreach (AppDomainConfigModel model in configModelList)
            {
                AppDomain aDomain = CreateAppDomain(model);
                var runner = CreateObject(aDomain, model);
                //use for unload appDomain
                _domainDict.Add(runner, aDomain);

                Dictionary<string, object> appDomainParamDict = new Dictionary<string, object>();
                appDomainParamDict.Add("CopyPath", model.CopyPath);
                appDomainParamDict.Add("AzureTableAccountInfo", Config.AzureTableAccountInfo);
                runner.DoWorkInShadowCopiedDomain(appDomainParamDict);
                runner.RunTask(appDomainParamDict);
            }

        }

        private static IMultAppDomain CreateObject(AppDomain domain, AppDomainConfigModel model)
        {
            Logger.Logger.DebugLog("Start create object", null);
            IMultAppDomain runner = null;
            try
            {
                runner = (IMultAppDomain)domain.CreateInstanceAndUnwrap(model.AssambleName, model.TypeName);
                Logger.Logger.DebugLog("Create object success", null);
            }catch(Exception ex)
            {
                Logger.Logger.ErrorLog("Create object error", ex.InnerException);
            }
            return runner;
        }

        private static AppDomain CreateAppDomain(AppDomainConfigModel model)
        {
            Logger.Logger.DebugLog("Start create appDomain", null);
            var cachePath = model.CachePath;
            var pluginPath = model.CopyPath;
            var domainPath = model.AppDomainName;
            if (!Directory.Exists(cachePath))
            {
                Directory.CreateDirectory(cachePath);
            }

            if (!Directory.Exists(pluginPath))
            {
                Directory.CreateDirectory(pluginPath);
            }

            string pirvateBasePath = String.Empty;
            string[] pluginPathArray = Directory.GetDirectories(pluginPath);
            if(pluginPathArray.Length > 0)
            {
                foreach(string pluginPathStr in pluginPathArray)
                {
                    if(Directory.Exists(System.IO.Path.Combine(pluginPathStr,"pluginDLL")))
                    {
                        string privatePath = System.IO.Path.Combine(pluginPathStr, "pluginDLL") + ";";
                        pirvateBasePath += privatePath;
                    }
                }
            }
            // This creates a ShadowCopy of the MEF DLL's (and any other DLL's in the ShadowCopyDirectories)
            var setup = new AppDomainSetup
            {
                CachePath = cachePath,
                ShadowCopyFiles = "true",
                ShadowCopyDirectories = pluginPath,
                ApplicationBase = domainPath,
                PrivateBinPath = String.IsNullOrWhiteSpace(pirvateBasePath) ? "" : pirvateBasePath.Substring(0, pirvateBasePath.Length - 1)
            };

            // Create a new AppDomain then create an new instance of this application in the new AppDomain.
            // This bypasses the Main method as it's not executing it.
            
            AppDomain domain = null;
            try
            {
                domain = AppDomain.CreateDomain(model.AppDomainName, AppDomain.CurrentDomain.Evidence, setup);
                Logger.Logger.DebugLog("Create appDomain success", null);
            }catch(Exception e)
            {
                Logger.Logger.ErrorLog("Create appDomain error", e.InnerException);
            }
            return domain;
        }

        private void CleanUpFolder(string FolderPath)
        {
            try
            {
                string[] dirArray = Directory.GetDirectories(FolderPath);
                if (dirArray.Length > 0)
                {
                    foreach (string folder in dirArray)
                    {
                        CleanUpFolder(folder);
                    }
                }

                string[] fileArray = Directory.GetFiles(FolderPath);
                if (fileArray.Length > 0)
                {
                    foreach (string file in fileArray)
                    {
                        File.Delete(file);
                    }
                    Directory.Delete(FolderPath);
                }
                else
                {
                    Directory.Delete(FolderPath);
                }
            }
            catch (Exception ex)
            {
                Logger.Logger.ErrorLog("Cleanup file error", ex.InnerException);
            }

        }

    }
}
