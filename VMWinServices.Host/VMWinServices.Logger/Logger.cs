﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace VMWinServices.Logger
{
    
    public class Logger
    {
        public static readonly log4net.ILog loginfo = log4net.LogManager.GetLogger("loginfo");

        public static readonly log4net.ILog logerror = log4net.LogManager.GetLogger("logerror");

        public static void WriteLog(string info)
        {
            if (loginfo.IsInfoEnabled)
            {
                loginfo.Info(info);
            }
        }
        /// <summary>
        /// Error log
        /// </summary>
        /// <param name="info">Additional information</param>
        /// <param name="ex">Error exception</param>
        public static void ErrorLog(string info, Exception ex)
        {
            if (!string.IsNullOrEmpty(info) && ex == null)
            {
                logerror.ErrorFormat("[Additional Information] : {0}<br>", new object[] { info });
            }
            else if (!string.IsNullOrEmpty(info) && ex != null)
            {
                string errorMsg = BeautyErrorMsg(ex);
                logerror.ErrorFormat("[Additional Information] : {0}<br>{1}", new object[] { info, errorMsg });
            }
            else if (string.IsNullOrEmpty(info) && ex != null)
            {
                string errorMsg = BeautyErrorMsg(ex);
                logerror.Error(errorMsg);
            }
        }

        /// <summary>
        /// Error log
        /// </summary>
        /// <param name="info">Additional information</param>
        /// <param name="ex">Error exception</param>
        public static void DebugLog(string info, Exception ex)
        {
            if (!string.IsNullOrEmpty(info) && ex == null)
            {
                loginfo.DebugFormat("[Additional Information] : {0}<br>", new object[] { info });
            }
            else if (!string.IsNullOrEmpty(info) && ex != null)
            {
                string debugMsg = BeautyErrorMsg(ex);
                loginfo.DebugFormat("[Additional Information] : {0}<br>{1}", new object[] { info, debugMsg });
            }
            else if (string.IsNullOrEmpty(info) && ex != null)
            {
                string debugMsg = BeautyErrorMsg(ex);
                loginfo.Debug(debugMsg);
            }
        }

        /// <summary>
        /// Error log
        /// </summary>
        /// <param name="info">Additional information</param>
        /// <param name="ex">Error exception</param>
        public static void WarnLog(string info, Exception ex)
        {
            if (!string.IsNullOrEmpty(info) && ex == null)
            {
                logerror.WarnFormat("[Additional Information] : {0}<br>", new object[] { info });
            }
            else if (!string.IsNullOrEmpty(info) && ex != null)
            {
                string WarnMsg = BeautyErrorMsg(ex);
                logerror.WarnFormat("[Additional Information] : {0}<br>{1}", new object[] { info, WarnMsg });
            }
            else if (string.IsNullOrEmpty(info) && ex != null)
            {
                string WarnMsg = BeautyErrorMsg(ex);
                logerror.Warn(WarnMsg);
            }
        }

        /// <summary>
        /// 美化错误信息
        /// </summary>
        /// <param name="ex">异常</param>
        /// <returns>错误信息</returns>
        private static string BeautyErrorMsg(Exception ex)
        {
            string errorMsg = string.Format("[Exception Type] : {0} <br>[Exception Information] : {1} <br>[Call Track] : {2}", new object[] { ex.GetType().Name, ex.Message, ex.StackTrace });
            errorMsg = errorMsg.Replace("\r\n", "<br>");
            errorMsg = errorMsg.Replace("Site", "<strong style=\"color:red\">Site</strong>");
            return errorMsg;
        }
    }
}
