﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VMWinServices.PlugInInterface;

namespace VMWinServices.PlugIn.WordConvert
{
    public class WordToImageExecute : AbstractExecute
    {
        public WordToImageExecute()
        {
            this.runTaskList = new List<Task>();
        }
        
        public override void OnStart(ExecuteArgs args)
        {
            System.Timers.Timer _timer = new System.Timers.Timer(3000);
            _timer.Elapsed += _timer_Elapsed;
            _timer.Start();
            
        }

        void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Task task1 = new Task(OnRunTask);
            task1.Start();
            this.runTaskList.Add(task1);
            
        }

        private void OnRunTask()
        {
            System.IO.File.AppendAllText(@"D:\log1.txt", "Start Task");
            System.Threading.Thread.Sleep(2000);
        }

    }
}
