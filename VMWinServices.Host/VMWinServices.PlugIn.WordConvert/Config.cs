﻿/* ==============================================================================
 * Description：Config  
 * Author：peytonzhang
 * Created：5/19/2016 11:46:14 AM
 * ==============================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMWinServices.PlugIn.WordConvert
{
    public class Config
    {
        private readonly string _assemblePath;
        public readonly Dictionary<string, string> _configParamsDict = null;
        public Config(Type t)
        {
            _assemblePath = t.Assembly.CodeBase.Replace("file:///", "");
            _configParamsDict = new Dictionary<string, string>();
            _configParamsDict.Add("TaskSplitTimer", GetConfigItem("TaskSplitTimer"));
            _configParamsDict.Add("DataConnectionString", GetConfigItem("DataConnectionString"));
            _configParamsDict.Add("CentralDataProvider", GetConfigItem("CentralDataProvider"));
            _configParamsDict.Add("BlobStorage", GetConfigItem("BlobStorage"));
            _configParamsDict.Add("BlobFileSystemPath", GetConfigItem("BlobFileSystemPath"));
            _configParamsDict.Add("TempFilePath", GetConfigItem("TempFilePath"));
            _configParamsDict.Add("DatabaseServer", GetConfigItem("DatabaseServer"));
            _configParamsDict.Add("fondInstallTaskTimer", GetConfigItem("fondInstallTaskTimer"));
            _configParamsDict.Add("EnableUploadProcessing", GetConfigItem("EnableUploadProcessing"));
            _configParamsDict.Add("maxRetryTime", GetConfigItem("maxRetryTime"));
            _configParamsDict.Add("SendEmailOnErrorEmailAddress", GetConfigItem("SendEmailOnErrorEmailAddress"));
            _configParamsDict.Add("SendEmailOnError", GetConfigItem("SendEmailOnError"));
            _configParamsDict.Add("AKVId", GetConfigItem("AKVId"));
            _configParamsDict.Add("AKVUri", GetConfigItem("AKVUri"));
            _configParamsDict.Add("AKVCertThumbprint", GetConfigItem("AKVCertThumbprint"));
            _configParamsDict.Add("AKVSecondaryId", GetConfigItem("AKVSecondaryId"));
            _configParamsDict.Add("AKVSecondaryUri", GetConfigItem("AKVSecondaryUri"));
            _configParamsDict.Add("AKVSecondaryCertThumbprint", GetConfigItem("AKVSecondaryCertThumbprint"));
        }

        public string GetConfigItem(string key)
        {
            System.Configuration.Configuration cfg = System.Configuration.ConfigurationManager.OpenExeConfiguration(_assemblePath);
            string ret;
            try
            {
                ret = cfg.AppSettings.Settings[key].Value;
            }
            catch (Exception)
            {
                return null;
            }

            return ret;
        }

    }
}
