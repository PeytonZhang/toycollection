﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMWinServices.PlugInInterface
{
    public interface IExecute
    {
        void OnStart(ExecuteArgs args);

        void DoNotCreateNewJob(ExecuteArgs args);

        bool IsComplatedAllJob();
    }
}
