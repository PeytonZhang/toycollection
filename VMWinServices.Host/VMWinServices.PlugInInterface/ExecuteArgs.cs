﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMWinServices.PlugInInterface
{
    public class ExecuteArgs
    {
        public Dictionary<string, object> ConfigDict { get; set; }

        public bool IsNotCreateNewJob { get; set; }
    }
}
