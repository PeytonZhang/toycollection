﻿/* ==============================================================================
 * Description：AbstractExecute  
 * Author：Peytonzhang
 * Created：5/17/2016 3:00:28 PM
 * ==============================================================================*/

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMWinServices.PlugInInterface
{
    public abstract class AbstractExecute : IExecute
    {
        protected static bool IsNotCreateNewJob { get; set; }
        protected List<Task> runTaskList { get; set; }
        public abstract void OnStart(ExecuteArgs args);

        public void DoNotCreateNewJob(ExecuteArgs args)
        {
            IsNotCreateNewJob = true;
        }

        public bool IsComplatedAllJob()
        {
            return IsNotCreateNewJob && runTaskList.All(a => a.IsCompleted);
        }
    }
}
