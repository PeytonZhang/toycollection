﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMWinServices.DomainInterface
{
    public class AppDomainConfigModel : MarshalByRefObject
    {
        public string CachePath { get; set; }

        public string CopyPath { get; set; }

        public string AppDomainName { get; set; }

        public string AssambleName { get; set; }

        public string TypeName { get; set; }
    }
}
