﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMWinServices.DomainInterface
{
    public interface IMultAppDomain
    {
        void DoWorkInShadowCopiedDomain(Dictionary<string,object> appDomainParamDict);

        void RunTask(Dictionary<string, object> appDomainParamDict);

        bool IsReloadAppDomain();
    }
}
