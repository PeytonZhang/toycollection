﻿/* ==============================================================================
 * Description：AppDomainConfigFactory  
 * Author：Peytonzhang
 * Created：5/16/2016 6:26:28 PM
 * ==============================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace VMWinServices.DomainInterface
{
    public class AppDomainConfigFactory
    {
        private readonly string _configPath;

        public AppDomainConfigFactory(string configPath)
        {
            _configPath = configPath;
        }

        public List<AppDomainConfigModel> CreateAllConfig()
        {
            List<AppDomainConfigModel> configModelList = new List<AppDomainConfigModel>();
            XDocument xDoc = XDocument.Load(_configPath);
            IEnumerable<XElement> xele = xDoc.Root.Elements();
            foreach (var xelement in xele.ToList())
            {
                var configModel = new AppDomainConfigModel
                {
                    AppDomainName = xelement.Attribute("DomainName").Value
                };
                var childxele = xelement.Elements().ToList();

                foreach (var nodexele in childxele)
                {
                    switch (nodexele.Name.LocalName)
                    {
                        case "DomainAssamblePath":
                            configModel.CopyPath = String.IsNullOrWhiteSpace(nodexele.Value) ? System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PlugIns") : System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, nodexele.Value);
                            break;
                        case "DomainCachePath":
                            configModel.CachePath = String.IsNullOrWhiteSpace(nodexele.Value) ? System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ShadowCopyCache") : System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, nodexele.Value);
                            break;
                        case "DomainAssambleName":
                            configModel.AssambleName = nodexele.Value;
                            break;
                        case "DomainTypeName":
                            configModel.TypeName = nodexele.Value;
                            break;
                    }
                }
                configModelList.Add(configModel);
            }


            return configModelList;
        }

        public AppDomainConfigModel CreateConfig(string appDomainName)
        {
            var configModel = new AppDomainConfigModel();
            XDocument xDoc = XDocument.Load(_configPath);
            if (xDoc.Root == null) return null;

            IEnumerable<XElement> xele = xDoc.Root.Elements();
            foreach (var xelement in xele.ToList())
            {
                if (xelement.Attribute("DomainName").Value == appDomainName)
                {
                    configModel.AppDomainName = xelement.Attribute("DomainName").Value;
                    foreach (var nodexele in xelement.Elements().ToList())
                    {
                        switch (nodexele.Name.LocalName)
                        {
                            case "DomainAssamblePath":
                                configModel.CachePath = String.IsNullOrWhiteSpace(nodexele.Value) ? System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PlugIns") : System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, nodexele.Value);
                                break;
                            case "DomainCachePath":
                                configModel.CachePath = String.IsNullOrWhiteSpace(nodexele.Value) ? System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ShadowCopyCache") : System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, nodexele.Value);
                                break;
                            case "DomainAssambleName":
                                configModel.AssambleName = nodexele.Value;
                                break;
                            case "DomainTypeName":
                                configModel.TypeName = nodexele.Value;
                                break;
                        }
                    }
                }
            }
            return configModel;
        }
    }
}
