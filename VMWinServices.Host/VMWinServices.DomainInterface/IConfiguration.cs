﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMWinServices.DomainInterface
{
    public interface IConfiguration
    {
        string getConfigItem(string key);
    }
}
