﻿using Ionic.Zip;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using Microsoft.WindowsAzure.StorageClient.Protocol;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace VMWinServices.AzureBlobHelper
{
    public class DownLoadBlobs
    {
        private readonly string _localFolder;
        public DownLoadBlobs(string localFolder)
        {
            _localFolder = localFolder;
            if (!System.IO.Directory.Exists(_localFolder))
            {
                System.IO.Directory.CreateDirectory(_localFolder);
            }
        }

        public string DownloadToLocalFile(string containerName, string blobName, string blobKey)
        {
            string blobPath = System.IO.Path.Combine(_localFolder, blobName);
            CloudBlockBlob blockBlob = null;
            try
            {
                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(blobKey);

                // Create the blob client.
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve reference to a previously created container.
                CloudBlobContainer container = blobClient.GetContainerReference(containerName);

                // Retrieve reference to a blob named "photo1.jpg".
                blockBlob = container.GetBlockBlobReference(blobName);



                if (System.IO.File.Exists(blobPath))
                    System.IO.File.Delete(blobPath);
                // Save blob contents to a file.
                ParallelDownloadFile(blockBlob, blobPath);
                
            }
            catch (Exception ex)
            {
                Logger.Logger.ErrorLog("Download PlugIn error", ex);
                return "";
            }
            if (System.IO.File.ReadAllBytes(blobPath).LongLength == blockBlob.Properties.Length)
            {
                try
                {
                    return UnzipBlobFile(blobPath);
                }catch(Exception ex)
                {
                    return "";
                }
                
            }
            else return "";


        }

        private void ParallelDownloadFile(CloudBlockBlob m_Blob, string m_FileName)
        {
            int numThreads = 10;
            try
            {
                m_Blob.FetchAttributes();
            }catch(Exception)
            {

            }
            long blobLength = m_Blob.Properties.Length;

            int bufferLength = GetBlockSize(blobLength);  // 4 * 1024 * 1024;
            long bytesDownloaded = 0;

            // Prepare a queue of chunks to be downloaded. Each queue item is a key-value pair 
            // where the 'key' is start offset in the blob and 'value' is the chunk length.
            Queue<KeyValuePair<long, int>> queue = new Queue<KeyValuePair<long, int>>();
            long offset = 0;
            while (blobLength > 0)
            {
                int chunkLength = (int)Math.Min(bufferLength, blobLength);
                queue.Enqueue(new KeyValuePair<long, int>(offset, chunkLength));
                offset += chunkLength;
                blobLength -= chunkLength;
            }

            int exceptionCount = 0;

            FileStream fs = new FileStream(m_FileName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read);

            using (fs)
            {
                // Launch threads to download chunks.
                List<Thread> threads = new List<Thread>();
                for (int idxThread = 0; idxThread < numThreads; idxThread++)
                {
                    Thread t = new Thread(new ThreadStart(() =>
                    {
                        KeyValuePair<long, int> blockIdAndLength;

                        // A buffer to fill per read request.
                        byte[] buffer = new byte[bufferLength];

                        while (true)
                        {
                            // Dequeue block details.
                            lock (queue)
                            {
                                if (queue.Count == 0)
                                    break;

                                blockIdAndLength = queue.Dequeue();
                            }

                            try
                            {
                                // Prepare the HttpWebRequest to download data from the chunk.
                                HttpWebRequest blobGetRequest = BlobRequest.Get(m_Blob.Uri, 60, null, null);

                                // Add header to specify the range
                                blobGetRequest.Headers.Add("x-ms-range", string.Format(System.Globalization.CultureInfo.InvariantCulture, "bytes={0}-{1}", blockIdAndLength.Key, blockIdAndLength.Key + blockIdAndLength.Value - 1));

                                // Sign request.
                                StorageCredentials credentials = m_Blob.ServiceClient.Credentials;
                                credentials.SignRequest(blobGetRequest);

                                // Read chunk.
                                using (HttpWebResponse response = blobGetRequest.GetResponse() as
                                    HttpWebResponse)
                                {
                                    using (Stream stream = response.GetResponseStream())
                                    {
                                        int offsetInChunk = 0;
                                        int remaining = blockIdAndLength.Value;
                                        while (remaining > 0)
                                        {
                                            int read = stream.Read(buffer, offsetInChunk, remaining);
                                            lock (fs)
                                            {
                                                fs.Position = blockIdAndLength.Key + offsetInChunk;
                                                fs.Write(buffer, offsetInChunk, read);
                                            }
                                            offsetInChunk += read;
                                            remaining -= read;
                                            System.Threading.Interlocked.Add(ref bytesDownloaded, read);
                                        }

                                        int progress = (int)((double)bytesDownloaded / m_Blob.Attributes.Properties.Length * 100);

                                        // raise the progress changed event
                                        //eArgs = new BlobTransferProgressChangedEventArgs(bytesDownloaded, m_Blob.Attributes.Properties.Length, progress, CalculateSpeed(bytesDownloaded), null);
                                        //asyncOp.Post(delegate(object e) { OnTaskProgressChanged((BlobTransferProgressChangedEventArgs)e); }, eArgs);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                // Add block back to queue
                                queue.Enqueue(blockIdAndLength);

                                exceptionCount++;
                                // If we have had more than 100 exceptions then break
                                if (exceptionCount == 100)
                                {
                                    throw new Exception("Received 100 exceptions while downloading. Cancelling download.   " + ex.ToString());
                                }
                                if (exceptionCount >= 100)
                                {
                                    break;
                                }
                            }
                        }
                    }));
                    t.Start();
                    threads.Add(t);
                }


                // Wait for all threads to complete downloading data.
                foreach (Thread t in threads)
                {
                    t.Join();
                }
            }
        }

        private int GetBlockSize(long fileSize)
        {
            const long KB = 1024;
            const long MB = 1024 * KB;
            const long GB = 1024 * MB;
            const long MAXBLOCKS = 50000;
            const long MAXBLOBSIZE = 200 * GB;
            const long MAXBLOCKSIZE = 4 * MB;

            long blocksize = 100 * KB;
            //long blocksize = 4 * MB;
            long blockCount;
            blockCount = ((int)Math.Floor((double)(fileSize / blocksize))) + 1;
            while (blockCount > MAXBLOCKS - 1)
            {
                blocksize += 100 * KB;
                blockCount = ((int)Math.Floor((double)(fileSize / blocksize))) + 1;
            }

            if (blocksize > MAXBLOCKSIZE)
            {
                throw new ArgumentException("Blob too big to upload.");
            }

            return (int)blocksize;
        }


        private string UnzipBlobFile(string blobPath)
        {
            string plugInUnzipFolder = System.IO.Path.Combine(_localFolder, "plugInUnzipFolder");
            try
            {
                ZipFile zip = new ZipFile(blobPath);
                
                if (!System.IO.Directory.Exists(plugInUnzipFolder))
                {
                    System.IO.Directory.CreateDirectory(plugInUnzipFolder);
                }
                else
                {
                    CleanUpFolder(plugInUnzipFolder);
                }
                zip.ExtractAll(plugInUnzipFolder);
                zip.Dispose();
                if (System.IO.File.Exists(blobPath))
                    System.IO.File.Delete(blobPath);
                
            }
            catch(Exception ex)
            {
                Logger.Logger.ErrorLog("unzip appear error", ex);
                throw;
            }
            return plugInUnzipFolder;
        }

        private void CleanUpFolder(string FolderPath)
        {
            try
            {
                string[] dirArray = Directory.GetDirectories(FolderPath);
                if (dirArray.Length > 0)
                {
                    foreach (string folder in dirArray)
                    {
                        CleanUpFolder(folder);
                    }
                }

                string[] fileArray = Directory.GetFiles(FolderPath);
                if (fileArray.Length > 0)
                {
                    foreach (string file in fileArray)
                    {
                        File.Delete(file);
                    }
                    Directory.Delete(FolderPath);
                }
                else
                {
                    Directory.Delete(FolderPath);
                }
            }
            catch (Exception ex)
            {
                Logger.Logger.ErrorLog("Cleanup file error", ex.InnerException);
            }

        }
    }
}
