﻿/* ==============================================================================
 * Description：InstanceStatusEnum  
 * Author：peytonzhang
 * Created：5/19/2016 10:42:38 AM
 * ==============================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMWinServices.AzureTableHelper
{
    public enum InstanceStatusEnum
    {
        Disabled,
        Enable,
        Destroy
    }
}
