﻿/* ==============================================================================
 * Description：InstanceConfig  
 * Author：Peytonzhang
 * Created：5/17/2016 11:44:26 AM
 * ==============================================================================*/

using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMWinServices.AzureTableHelper.Model
{
    public class InstanceConfig : TableEntity
    {
        public InstanceConfig(string partitionKey, string rowKey)
        {
            RowKey = String.IsNullOrEmpty(rowKey) ? Guid.NewGuid().ToString() : rowKey;
            PartitionKey = String.IsNullOrEmpty(partitionKey) ? Guid.NewGuid().ToString() : partitionKey; ;
        }

        public string InstanceName { get; set; }

        public string InstanceStatus { get; set; }

        public string DataCenterConfigName { get; set; }
    }
}
