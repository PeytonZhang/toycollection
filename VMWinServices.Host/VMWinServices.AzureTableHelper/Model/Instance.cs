﻿/* ==============================================================================
 * Description：Instance  
 * Author：peytonzhang
 * Created：5/18/2016 11:15:10 AM
 * ==============================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMWinServices.AzureTableHelper.Model
{
    public class Instance
    {
        public string PartitionKey { get; set; }
        public string RowKey {get;set;}
        public string InstanceName { get; set; }
        public string InstanceStatus { get; set; }
        public string DataCenterConfigName { get; set; }
    }
}
