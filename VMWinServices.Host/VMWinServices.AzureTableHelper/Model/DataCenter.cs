﻿/* ==============================================================================
 * Description：DataCenter  
 * Author：peytonzhang
 * Created：5/18/2016 11:15:03 AM
 * ==============================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMWinServices.AzureTableHelper.Model
{
    public class DataCenter
    {
        public string RowKey { get; set; }
        public string PartitionKey { get; set; }
        public string ConfigName { get;set;}
        public string PlugInConfigXml { get; set; }
        public string BlobStorageAccount { get; set; }
    }
}
