﻿/* ==============================================================================
 * Description：PlugInConfig  
 * Author：Peytonzhang
 * Created：5/17/2016 11:46:07 AM
 * ==============================================================================*/

using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMWinServices.AzureTableHelper.Model
{
    public class DataCenterConfig : TableEntity
    {
        public DataCenterConfig(string partitionKey, string rowKey)
        {
            RowKey = String.IsNullOrEmpty(rowKey) ? Guid.NewGuid().ToString() : rowKey;
            PartitionKey = String.IsNullOrEmpty(partitionKey) ? Guid.NewGuid().ToString() : partitionKey; ;
        }

        public string Configname { get; set; }

        public string PlugInConfigXml { get; set; }

        public string BlobStorageAccount { get; set; }
    }
}
