﻿/* ==============================================================================
 * Description：AzureTableBase  
 * Author：Peytonzhang
 * Created：5/17/2016 11:24:30 AM
 * ==============================================================================*/

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMWinServices.AzureTableHelper.DbFactory
{
    public class AzureTableBase
    {
        protected CloudTable table;

        public AzureTableBase(string tableName, string dataConnectionString)
        {
            string azureDataConnectionString = dataConnectionString;// ConfigUtil.GetConfigItem("DataConnectionString");
            CloudStorageAccount account = CloudStorageAccount.Parse(azureDataConnectionString);

            CloudTableClient cloudTableClient = account.CreateCloudTableClient();
            table = cloudTableClient.GetTableReference(tableName);
            table.CreateIfNotExists();
        }

        public List<T> GetObjectList<T>(EntityResolver<T> resolver)
        {
            TableQuery<DynamicTableEntity> query = new TableQuery<DynamicTableEntity>();
            List<T> tableEntityList = table.ExecuteQuery(query, resolver, null, null).ToList<T>();
            return tableEntityList;
        }

        public void SaveAzureObject(TableEntity entity)
        {
            try
            {
                TableOperation insertOperation = TableOperation.Insert(entity);
                table.Execute(insertOperation);
            }catch(Exception ex)
            {
                throw ex.InnerException;
            }
            
        }

        public void SaveOrUpdate(TableEntity entity)
        {
            TableOperation insertOperation = TableOperation.InsertOrReplace(entity);
            table.Execute(insertOperation);
            
        }

        public void UpdateAzureObject(TableEntity entity)
        {
            TableOperation deleteOperation = TableOperation.Replace(entity);
            table.Execute(deleteOperation);
        }

        public void DeleteAzureObject(TableEntity entity)
        {
            if (entity.ETag == null) entity.ETag = "*";


            TableOperation deleteOperation = TableOperation.Delete(entity);

            // Execute the operation.
            table.Execute(deleteOperation);


        }
    }
}
