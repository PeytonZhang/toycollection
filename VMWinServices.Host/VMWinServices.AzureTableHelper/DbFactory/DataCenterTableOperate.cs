﻿/* ==============================================================================
 * Description：DateCenterTableOperate  
 * Author：peytonzhang
 * Created：5/18/2016 4:12:34 PM
 * ==============================================================================*/

using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMWinServices.AzureTableHelper.Model;

namespace VMWinServices.AzureTableHelper.DbFactory
{
    public class DataCenterTableOperate
    {
        private readonly AzureTableBase _tableBase;
        public DataCenterTableOperate(string tableName, string dataConnectionString)
        {
            _tableBase = new AzureTableBase(tableName, dataConnectionString);
        }

        public void SaveDateCenterTable(DataCenter instance)
        {
            SaveOrUpdate(instance);
        }

        public void SaveOrUpdate(DataCenter instance)
        {
            List<DataCenterConfig> _inList = getEntityList();
            if (_inList == null)
                return;
            DataCenterConfig instanceCon;
            if (_inList.Where(w => w.Configname == instance.ConfigName).ToList().Count > 0)
            {
                instanceCon = _inList.Where(w => w.Configname == instance.ConfigName).First();
                instanceCon.BlobStorageAccount = instance.BlobStorageAccount;
                instanceCon.PlugInConfigXml = instance.PlugInConfigXml;
            }
            else
            {
                instanceCon = ConvertDataCenterToDataCenterConfig(instance);
            }
            _tableBase.SaveOrUpdate(instanceCon);
        }

        public void UpdateDateCenterTable(DataCenter instance)
        {
            List<DataCenterConfig> _inList = getEntityList();
            if (_inList == null)
                return;
            DataCenterConfig instanceCon;
            if (_inList.Where(w => w.Configname == instance.ConfigName).ToList().Count > 0)
            {
                instanceCon = _inList.Where(w => w.Configname == instance.ConfigName).First();
                instanceCon.BlobStorageAccount = instance.BlobStorageAccount;
                instanceCon.PlugInConfigXml = instance.PlugInConfigXml;
                _tableBase.UpdateAzureObject(instanceCon);
            }
            else
            {
                throw new Exception("Record is not in azure table, Can not update record");
            }
        }

        public void DeleteDateCenterTable(DataCenter instance)
        {
            List<DataCenterConfig> tableEntityList = getEntityList();

            if (tableEntityList == null)
                return;
            if (tableEntityList.Where(w => w.Configname == instance.ConfigName).ToList().Count > 0)
            {
                DataCenterConfig instanceConfig = tableEntityList.Where(w => w.Configname == instance.ConfigName).ToList().First();
                _tableBase.DeleteAzureObject(instanceConfig);                                
            }
        }

        public List<DataCenter> getObjectList()
        {

            List<DataCenter> resultInstance = new List<DataCenter>();

            List<DataCenterConfig> tableEntityList = getEntityList();

            if (tableEntityList == null)
                return null;
            foreach (DataCenterConfig tableEntity in tableEntityList)
            {
                resultInstance.Add(ConvertDataCenterConfigToDataCenter(tableEntity));
            }
            return resultInstance;
        }

        public DataCenter getObjectListByDataCenterConfigName(string DataCenterConfigName)
        {
            List<DataCenter> resultInstance = getObjectList();

            if (resultInstance.Where(w => w.ConfigName == DataCenterConfigName).ToList().Count > 0)
                return resultInstance.Where(w => w.ConfigName == DataCenterConfigName).First();
            else return null;
        }

        private List<DataCenterConfig> getEntityList()
        {
            EntityResolver<DataCenterConfig> resolver = getResolver;
            List<DataCenterConfig> tableEntityList = _tableBase.GetObjectList<DataCenterConfig>(resolver);

            return tableEntityList;
        }

        private DataCenterConfig getResolver(string pk, string rk, System.DateTimeOffset ts, IDictionary<string, EntityProperty> props, string etag)
        {
            DataCenterConfig dataCenter = new DataCenterConfig(pk,rk);
            dataCenter.PlugInConfigXml = props.ContainsKey("PlugInConfigXml") ? props["PlugInConfigXml"].StringValue : null;
            dataCenter.BlobStorageAccount = props.ContainsKey("BlobStorageAccount") ? props["BlobStorageAccount"].StringValue : null;
            dataCenter.Configname = props.ContainsKey("Configname") ? props["Configname"].StringValue : null;
            return dataCenter;
        }

        private DataCenterConfig ConvertDataCenterToDataCenterConfig(DataCenter dataCenter)
        {
            if (dataCenter == null) return null;
            return new DataCenterConfig(dataCenter.PartitionKey, dataCenter.RowKey) 
            {
                Configname = dataCenter.ConfigName, 
                PlugInConfigXml = dataCenter.PlugInConfigXml, 
                BlobStorageAccount = dataCenter.BlobStorageAccount 
            };
        }

        private DataCenter ConvertDataCenterConfigToDataCenter(DataCenterConfig dataCenterConfig)
        {
            if (dataCenterConfig == null) return null;
            return new DataCenter()
            {
                PartitionKey = dataCenterConfig.PartitionKey,
                RowKey = dataCenterConfig.RowKey,
                PlugInConfigXml = dataCenterConfig.PlugInConfigXml,
                ConfigName = dataCenterConfig.Configname,
                BlobStorageAccount = dataCenterConfig.BlobStorageAccount
            };
        }
    }
}
