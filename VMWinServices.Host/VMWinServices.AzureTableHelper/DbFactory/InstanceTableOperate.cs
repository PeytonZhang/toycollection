﻿/* ==============================================================================
 * Description：InstanceTableOperate  
 * Author：peytonzhang
 * Created：5/18/2016 10:57:14 AM
 * ==============================================================================*/

using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMWinServices.AzureTableHelper.Model;

namespace VMWinServices.AzureTableHelper.DbFactory
{
    public class InstanceTableOperate
    {
        private readonly AzureTableBase _tableBase;
        public InstanceTableOperate(string tableName, string dataConnectionString)
        {
            _tableBase = new AzureTableBase(tableName, dataConnectionString);
        }

        public void Save(Instance instance)
        {
            SaveOrUpdate(instance);
        }

        public void SaveOrUpdate(Instance instance)
        {
            List<InstanceConfig> _inList = getEntityList();
            if (_inList == null)
                return;
            InstanceConfig instanceCon;
            if (_inList.Where(w => w.InstanceName == instance.InstanceName).ToList().Count > 0)
            {
                instanceCon = _inList.Where(w => w.InstanceName == instance.InstanceName).First();
                instanceCon.InstanceStatus = instance.InstanceStatus;
                instanceCon.DataCenterConfigName = instance.DataCenterConfigName;
            }
            else
            {
                instanceCon = ConvertInstanceToInstanceConfig(instance);
            }
            _tableBase.SaveOrUpdate(instanceCon);
        }

        public void UpdateInstanceTable(Instance instance)
        {
            List<InstanceConfig> _inList = getEntityList();
            if (_inList == null)
                return;
            InstanceConfig instanceCon;
            if (_inList.Where(w => w.InstanceName == instance.InstanceName).ToList().Count > 0)
            {
                instanceCon = _inList.Where(w => w.InstanceName == instance.InstanceName).First();
                instanceCon.InstanceStatus = instance.InstanceStatus;
                instanceCon.DataCenterConfigName = instance.DataCenterConfigName;
                _tableBase.UpdateAzureObject(instanceCon);
            }
            else
            {
                throw new Exception("Record is not in azure table, Can not update record");
            }
            
        }

        public void DeleteInstanceTable(Instance instance)
        {
            List<InstanceConfig> tableEntityList = getEntityList();

            if (tableEntityList == null)
                return;
            if (tableEntityList.Where(w => w.InstanceName == instance.InstanceName).ToList().Count > 0)
            {
                InstanceConfig instanceConfig = tableEntityList.Where(w => w.InstanceName == instance.InstanceName).ToList().First();
                _tableBase.DeleteAzureObject(instanceConfig);
            }
            
        }

        public List<Instance> getObjectList()
        {
            List<Instance> resultInstance = new List<Instance>();
            List<InstanceConfig> tableEntityList = getEntityList();
            if (tableEntityList == null)
                return null;
            foreach (InstanceConfig tableEntity in tableEntityList)
            {
                resultInstance.Add(ConvertInstanceConfigToInstance(tableEntity));
            }
            return resultInstance;
        }

        public Instance getObjectListByInstanceName(string InstanceName)
        {
            List<Instance> resultInstance = getObjectList();
            if (resultInstance.Where(w => w.InstanceName == InstanceName).ToList().Count > 0)
                return resultInstance.Where(w => w.InstanceName == InstanceName).First();
            else return null;
        }

        private List<InstanceConfig> getEntityList()
        {
            EntityResolver<InstanceConfig> resolver = getResolver;
            List<InstanceConfig> tableEntityList = _tableBase.GetObjectList<InstanceConfig>(resolver);

            return tableEntityList;
        }

        private InstanceConfig getResolver(string pk, string rk, System.DateTimeOffset ts, IDictionary<string, EntityProperty> props, string etag)
        {
            InstanceConfig instance = new InstanceConfig(pk,rk);
            instance.InstanceStatus = props.ContainsKey("InstanceStatus") ? props["InstanceStatus"].StringValue : null;
            instance.DataCenterConfigName = props.ContainsKey("DataCenterConfigName") ? props["DataCenterConfigName"].StringValue : null;
            instance.InstanceName = props.ContainsKey("InstanceName") ? props["InstanceName"].StringValue : null;
            return instance;
        }

        private InstanceConfig ConvertInstanceToInstanceConfig(Instance instance)
        {
            if (instance == null) return null;
            return new InstanceConfig(instance.PartitionKey, instance.RowKey) 
            { 
                InstanceName = instance.InstanceName,
                InstanceStatus = instance.InstanceStatus,
                DataCenterConfigName = instance.DataCenterConfigName
            };
        }

        private Instance ConvertInstanceConfigToInstance(InstanceConfig instanceConfig)
        {
            if (instanceConfig == null) return null;
            return new Instance() 
            { 
                PartitionKey = instanceConfig.PartitionKey,
                RowKey = instanceConfig.RowKey,
                InstanceName = instanceConfig.InstanceName,
                InstanceStatus = instanceConfig.InstanceStatus, 
                DataCenterConfigName = instanceConfig.DataCenterConfigName 
            };
        }

    }
}
