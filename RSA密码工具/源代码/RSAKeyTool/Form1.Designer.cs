﻿namespace RSAKeyTool
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.cBoxRSALen = new System.Windows.Forms.ComboBox();
            this.gBoxItem = new System.Windows.Forms.GroupBox();
            this.btnCreateKey = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cBoxShowType = new System.Windows.Forms.ComboBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.生成密钥ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.加密解密ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labCopyResult = new System.Windows.Forms.LinkLabel();
            this.txtWord = new System.Windows.Forms.TextBox();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cBoxProcessType = new System.Windows.Forms.ComboBox();
            this.txtPublicKey = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPrivateKey = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.labCopyPublic = new System.Windows.Forms.LinkLabel();
            this.labCopyPrivate = new System.Windows.Forms.LinkLabel();
            this.gBoxItem.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "RSA密钥长度（bit）";
            // 
            // cBoxRSALen
            // 
            this.cBoxRSALen.FormattingEnabled = true;
            this.cBoxRSALen.Items.AddRange(new object[] {
            "1024",
            "2048",
            "3072",
            "7680",
            "15360"});
            this.cBoxRSALen.Location = new System.Drawing.Point(125, 20);
            this.cBoxRSALen.Name = "cBoxRSALen";
            this.cBoxRSALen.Size = new System.Drawing.Size(121, 20);
            this.cBoxRSALen.TabIndex = 1;
            this.cBoxRSALen.Text = "请选择密钥长度";
            // 
            // gBoxItem
            // 
            this.gBoxItem.Controls.Add(this.btnCreateKey);
            this.gBoxItem.Controls.Add(this.label2);
            this.gBoxItem.Controls.Add(this.label1);
            this.gBoxItem.Controls.Add(this.cBoxShowType);
            this.gBoxItem.Controls.Add(this.cBoxRSALen);
            this.gBoxItem.Location = new System.Drawing.Point(2, 32);
            this.gBoxItem.Name = "gBoxItem";
            this.gBoxItem.Size = new System.Drawing.Size(262, 134);
            this.gBoxItem.TabIndex = 2;
            this.gBoxItem.TabStop = false;
            this.gBoxItem.Text = "选项";
            // 
            // btnCreateKey
            // 
            this.btnCreateKey.Location = new System.Drawing.Point(77, 94);
            this.btnCreateKey.Name = "btnCreateKey";
            this.btnCreateKey.Size = new System.Drawing.Size(75, 23);
            this.btnCreateKey.TabIndex = 2;
            this.btnCreateKey.Text = "生成";
            this.btnCreateKey.UseVisualStyleBackColor = true;
            this.btnCreateKey.Click += new System.EventHandler(this.btnCreateKey_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "展示方式：";
            // 
            // cBoxShowType
            // 
            this.cBoxShowType.FormattingEnabled = true;
            this.cBoxShowType.Items.AddRange(new object[] {
            "生成到文件",
            "生成到界面"});
            this.cBoxShowType.Location = new System.Drawing.Point(125, 56);
            this.cBoxShowType.Name = "cBoxShowType";
            this.cBoxShowType.Size = new System.Drawing.Size(121, 20);
            this.cBoxShowType.TabIndex = 1;
            this.cBoxShowType.Text = "生成到界面";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.帮助ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(537, 25);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.生成密钥ToolStripMenuItem,
            this.加密解密ToolStripMenuItem});
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.帮助ToolStripMenuItem.Text = "帮助";
            // 
            // 生成密钥ToolStripMenuItem
            // 
            this.生成密钥ToolStripMenuItem.Name = "生成密钥ToolStripMenuItem";
            this.生成密钥ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.生成密钥ToolStripMenuItem.Text = "生成密钥";
            // 
            // 加密解密ToolStripMenuItem
            // 
            this.加密解密ToolStripMenuItem.Name = "加密解密ToolStripMenuItem";
            this.加密解密ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.加密解密ToolStripMenuItem.Text = "加密解密";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labCopyResult);
            this.groupBox1.Controls.Add(this.txtWord);
            this.groupBox1.Controls.Add(this.txtResult);
            this.groupBox1.Controls.Add(this.btnStart);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cBoxProcessType);
            this.groupBox1.Location = new System.Drawing.Point(2, 172);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(262, 234);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "选项";
            // 
            // labCopyResult
            // 
            this.labCopyResult.AutoSize = true;
            this.labCopyResult.Location = new System.Drawing.Point(193, 160);
            this.labCopyResult.Name = "labCopyResult";
            this.labCopyResult.Size = new System.Drawing.Size(53, 12);
            this.labCopyResult.TabIndex = 6;
            this.labCopyResult.TabStop = true;
            this.labCopyResult.Text = "复制结果";
            this.labCopyResult.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.labCopyResult_LinkClicked);
            // 
            // txtWord
            // 
            this.txtWord.Location = new System.Drawing.Point(53, 15);
            this.txtWord.Multiline = true;
            this.txtWord.Name = "txtWord";
            this.txtWord.Size = new System.Drawing.Size(193, 68);
            this.txtWord.TabIndex = 5;
            // 
            // txtResult
            // 
            this.txtResult.Location = new System.Drawing.Point(53, 89);
            this.txtResult.Multiline = true;
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(193, 68);
            this.txtResult.TabIndex = 4;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(53, 205);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "开始";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(-2, 171);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "处理方式";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "结果：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "密文：";
            // 
            // cBoxProcessType
            // 
            this.cBoxProcessType.FormattingEnabled = true;
            this.cBoxProcessType.Items.AddRange(new object[] {
            "解密",
            "加密"});
            this.cBoxProcessType.Location = new System.Drawing.Point(69, 168);
            this.cBoxProcessType.Name = "cBoxProcessType";
            this.cBoxProcessType.Size = new System.Drawing.Size(59, 20);
            this.cBoxProcessType.TabIndex = 1;
            this.cBoxProcessType.Text = "解密";
            // 
            // txtPublicKey
            // 
            this.txtPublicKey.Location = new System.Drawing.Point(286, 55);
            this.txtPublicKey.Multiline = true;
            this.txtPublicKey.Name = "txtPublicKey";
            this.txtPublicKey.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtPublicKey.Size = new System.Drawing.Size(239, 148);
            this.txtPublicKey.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(284, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 4;
            this.label7.Text = "公钥";
            // 
            // txtPrivateKey
            // 
            this.txtPrivateKey.Location = new System.Drawing.Point(286, 232);
            this.txtPrivateKey.Multiline = true;
            this.txtPrivateKey.Name = "txtPrivateKey";
            this.txtPrivateKey.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtPrivateKey.Size = new System.Drawing.Size(239, 174);
            this.txtPrivateKey.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(284, 209);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 4;
            this.label8.Text = "私钥";
            // 
            // labCopyPublic
            // 
            this.labCopyPublic.AutoSize = true;
            this.labCopyPublic.Location = new System.Drawing.Point(496, 32);
            this.labCopyPublic.Name = "labCopyPublic";
            this.labCopyPublic.Size = new System.Drawing.Size(29, 12);
            this.labCopyPublic.TabIndex = 5;
            this.labCopyPublic.TabStop = true;
            this.labCopyPublic.Text = "复制";
            this.labCopyPublic.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.labCopyPublic_LinkClicked);
            // 
            // labCopyPrivate
            // 
            this.labCopyPrivate.AutoSize = true;
            this.labCopyPrivate.Location = new System.Drawing.Point(496, 209);
            this.labCopyPrivate.Name = "labCopyPrivate";
            this.labCopyPrivate.Size = new System.Drawing.Size(29, 12);
            this.labCopyPrivate.TabIndex = 5;
            this.labCopyPrivate.TabStop = true;
            this.labCopyPrivate.Text = "复制";
            this.labCopyPrivate.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.labCopyPrivate_LinkClicked);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 418);
            this.Controls.Add(this.labCopyPrivate);
            this.Controls.Add(this.labCopyPublic);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtPrivateKey);
            this.Controls.Add(this.txtPublicKey);
            this.Controls.Add(this.gBoxItem);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RSA工具";
            this.gBoxItem.ResumeLayout(false);
            this.gBoxItem.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cBoxRSALen;
        private System.Windows.Forms.GroupBox gBoxItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cBoxShowType;
        private System.Windows.Forms.Button btnCreateKey;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 生成密钥ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 加密解密ToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cBoxProcessType;
        private System.Windows.Forms.TextBox txtPublicKey;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPrivateKey;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtWord;
        private System.Windows.Forms.LinkLabel labCopyPublic;
        private System.Windows.Forms.LinkLabel labCopyPrivate;
        private System.Windows.Forms.LinkLabel labCopyResult;
    }
}

