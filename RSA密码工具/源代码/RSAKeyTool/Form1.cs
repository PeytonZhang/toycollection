﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RSAHandler;

namespace RSAKeyTool
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCreateKey_Click(object sender, EventArgs e)
        {
            try
            {
                int len = this.cBoxRSALen.Text == "请选择密钥长度" ? 0 : int.Parse(this.cBoxRSALen.Text);
                string type = this.cBoxShowType.Text;
                RSAClass rsa = new RSAClass(len);
                string publicKey = rsa.CreatePublicKey();
                string privateKey = rsa.CreatePrivateKey();
                if (type == "生成到界面")
                {
                    this.txtPublicKey.Text = publicKey;
                    this.txtPrivateKey.Text = privateKey;
                }
                else
                {
                    SaveToFile("publickey.xml", publicKey);
                    SaveToFile("privatekey.xml", privateKey);
                    MessageBox.Show("请到软件安装目录下查找文件publickey.xml(公钥)和privatekey.xml(私钥)!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// 保存文件
        /// </summary>
        /// <param name="filename">文件路径</param>
        /// <param name="data">保存内容</param>
        private void SaveToFile(String filename, String data)
        {
            System.IO.StreamWriter sw = System.IO.File.CreateText(filename);
            sw.WriteLine(data);
            sw.Close();
        }

        private void labCopyPublic_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Clipboard.SetDataObject(this.txtPublicKey.Text.Trim(), true);
            MessageBox.Show("复制公钥成功！");
        }

        private void labCopyPrivate_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Clipboard.SetDataObject(this.txtPrivateKey.Text.Trim(), true);
            MessageBox.Show("复制私钥成功！");
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder sbuilder = new StringBuilder();
                if (txtWord.Text.Trim().Length == 0)
                {
                    sbuilder.Append("请输入要处理的密文！");
                }
                else if (cBoxProcessType.Text == "解密")
                {
                    if (txtPrivateKey.Text.Trim().Length == 0)
                    {
                        sbuilder.Append("\r\n请输入私钥！");
                    }
                    else
                    {
                        this.txtResult.Text = RSAProvider.GetDecrypt(txtWord.Text, txtPrivateKey.Text.Trim());
                    }
                }
                else
                {
                    if (txtPublicKey.Text.Trim().Length == 0)
                    {
                        sbuilder.Append("\r\n请输入公钥！");
                    }
                    else
                    {
                        this.txtResult.Text = RSAProvider.GetEncrypt(txtWord.Text, txtPublicKey.Text.Trim());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void labCopyResult_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Clipboard.SetDataObject(this.txtResult.Text, true);
            MessageBox.Show("复制结果成功！");
        }  
    }
}
