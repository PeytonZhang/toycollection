﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Configuration;

namespace RSAHandler
{
    public class RSAProvider
    {
        //shaoguang系统简码
        /// <summary>
        /// 根据私钥生成解密管道。
        /// </summary>
        /// <param name="privateKeyFile">存放私钥的xml文件路径</param>
        /// <returns></returns>
        private static RSACryptoServiceProvider CreateRSAProvider(String privateKey)
        {
            RSAParameters parameters1;
            parameters1 = new RSAParameters();
            //StreamReader reader1 = new StreamReader(privateKeyFile);
            XmlDocument document1 = new XmlDocument();
            document1.LoadXml(privateKey);
            XmlElement element1 = (XmlElement)document1.SelectSingleNode("root");
            parameters1.Modulus = ReadChild(element1, "Modulus");
            parameters1.Exponent = ReadChild(element1, "Exponent");
            parameters1.D = ReadChild(element1, "D");
            parameters1.DP = ReadChild(element1, "DP");
            parameters1.DQ = ReadChild(element1, "DQ");
            parameters1.P = ReadChild(element1, "P");
            parameters1.Q = ReadChild(element1, "Q");
            parameters1.InverseQ = ReadChild(element1, "InverseQ");
            CspParameters parameters2 = new CspParameters();
            parameters2.Flags = CspProviderFlags.UseMachineKeyStore;
            RSACryptoServiceProvider provider1 = new RSACryptoServiceProvider(parameters2);
            provider1.ImportParameters(parameters1);
            return provider1;
        }
        /// <summary>
        /// 根据公钥生成加密管道。
        /// </summary>
        /// <param name="publicKeyFile">存放公钥的xml文件路径</param>
        /// <returns></returns>
        private static RSACryptoServiceProvider CreateRSAEncryptProvider(String publicKey)
        {
            RSAParameters parameters1;
            parameters1 = new RSAParameters();
            //StreamReader reader1 = new StreamReader(publicKeyFile);
            XmlDocument document1 = new XmlDocument();
            document1.LoadXml(publicKey);
            XmlElement element1 = (XmlElement)document1.SelectSingleNode("root");
            parameters1.Modulus = ReadChild(element1, "Modulus");
            parameters1.Exponent = ReadChild(element1, "Exponent");
            CspParameters parameters2 = new CspParameters();
            parameters2.Flags = CspProviderFlags.UseMachineKeyStore;
            RSACryptoServiceProvider provider1 = new RSACryptoServiceProvider(parameters2);
            provider1.ImportParameters(parameters1);
            return provider1;
        }

        /// <summary>
        /// 加密方法
        /// </summary>
        /// <param name="data">用户密码（明文）</param>
        public static string GetEncrypt(string password,string publicKey)
        {
            byte[] data = new UnicodeEncoding().GetBytes(password);
            //string path = ConfigurationManager.AppSettings["publicKeyPath"].ToString();
            RSACryptoServiceProvider rsaprd = CreateRSAEncryptProvider(publicKey);
            byte[] endata = rsaprd.Encrypt(data, true);
            return Convert.ToBase64String(endata);
        }

        /// <summary>
        /// 解密方法
        /// </summary>
        /// <param name="data">用户密码（暗文）</param>
        public static string GetDecrypt(string password,string publicKey)
        {
            byte[] data = Convert.FromBase64String(password);
            //string path = ConfigurationManager.AppSettings["privateKeyPath"].ToString();
            RSACryptoServiceProvider rsaprd = CreateRSAProvider(publicKey);
            byte[] dedata = rsaprd.Decrypt(data, true);
            return (new UnicodeEncoding()).GetString(dedata);
        }

        /// <summary>
        /// 解析XML中指定节点的内容
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        private static byte[] ReadChild(XmlElement parent, string name)
        {
            XmlElement element1 = (XmlElement)parent.SelectSingleNode(name);
            return Convert.FromBase64String(element1.InnerText);
        }
    }
}
