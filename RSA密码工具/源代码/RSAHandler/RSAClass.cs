﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace RSAHandler
{
    public class RSAClass
    {
        RSACryptoServiceProvider rsa;
        /// <summary>
        /// 根据密钥长度生成密钥（长度分为：1024、2048、3072、7680、15360）
        /// </summary>
        /// <param name="len"></param>
        public RSAClass(int len)
        {
            if (len == 0)
                rsa = new RSACryptoServiceProvider();
            else
                rsa = new RSACryptoServiceProvider(len);
        }
        public string CreatePublicKey()
        {            
            //将生成的密钥导出为RSA参数
            RSAParameters keys = rsa.ExportParameters(true);
            String pkxml = "<root>\n<Modulus>" + Convert.ToBase64String(keys.Modulus) + "</Modulus>";
            pkxml += "\n<Exponent>" + Convert.ToBase64String(keys.Exponent) + "</Exponent>\n</root>";
            return pkxml;
        }
        public string CreatePrivateKey()
        {
            //将生成的密钥导出为RSA参数
            RSAParameters keys = rsa.ExportParameters(true);
            String psxml = "<root>\n<Modulus>" + Convert.ToBase64String(keys.Modulus) + "</Modulus>";
            psxml += "\n<Exponent>" + Convert.ToBase64String(keys.Exponent) + "</Exponent>";
            psxml += "\n<D>" + Convert.ToBase64String(keys.D) + "</D>";
            psxml += "\n<DP>" + Convert.ToBase64String(keys.DP) + "</DP>";
            psxml += "\n<P>" + Convert.ToBase64String(keys.P) + "</P>";
            psxml += "\n<Q>" + Convert.ToBase64String(keys.Q) + "</Q>";
            psxml += "\n<DQ>" + Convert.ToBase64String(keys.DQ) + "</DQ>";
            psxml += "\n<InverseQ>" + Convert.ToBase64String(keys.InverseQ) + "</InverseQ>\n</root>";
            return psxml;
        }

    }
}
